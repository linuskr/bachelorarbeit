\documentclass{beamer}
\mode<presentation>
{
  \usetheme{myulm}
  \setbeamercovered{transparent}
  \setbeamertemplate{navigation symbols}{} % no navigation bar
  \setbeamersize{sidebar width left=1.17cm}
}

\usepackage[english]{babel}
\usepackage[utf8]{inputenc}
\usepackage{amsmath,amssymb,amsfonts}
\usepackage{times}
\usepackage{graphicx}
\usepackage{fancyvrb}
\usepackage{array}
\usepackage{colortbl}
\usepackage{xcolor}
\usepackage{tikz}
\usetikzlibrary{arrows, positioning, calc, overlay-beamer-styles, intersections, decorations.pathreplacing}
\usepackage{pgfplots}
\pgfplotsset{compat=newest}
\usepackage{ifthenx}
\usepackage{wrapfig}

\newcommand{\mbf}[1]{\mathbf{#1}}

\definecolor{pcolor1}{RGB}{152,78,163}
\definecolor{pcolor2}{RGB}{255,127,0}

% Anfang der Titelfolie
% Anpassung von: Titel, Untertitel, Autor, Datum und Institut

\title{Undistortion of Images\\behind Refracting Geometry}
\subtitle{}
\author{Linus Kramer}
\newcommand{\presdatum}{\today} % alternativ zu \today: Eingabe eines festen Datums
\institute{Institute of Media Informatics}
%Ende der Titelfolie

% Anfang der Kopfzeile der Folien
% Anpassung von: Zwischentitel, Leitthema oder Name
% Das Datum wird oben geändert: unter \presdatum{}!

\newcommand{\zwischentitel}{}
\newcommand{\leitthema}{Undistortion of Images behind Refracting Geometry}
% Ende der Kopfzeile

% Anfang der Folien
\begin{document}
\hspace*{-1.49cm}
\frame[plain]{\titlepage}

\section{Introduction}
\renewcommand{\zwischentitel}{Introduction}

\begin{frame}
\frametitle{Distortion due to Refraction}
\begin{tikzpicture}[overlay]
\draw[thick, color=blue!70!black, name path=S] (1, 0) -- (7, 0);
\draw[very thin, color=blue!70, ->] (3.5, 0) -- (3.5, 2.5);
\begin{scope}[thick]
\draw[yshift=2cm] (-15:1) -- (0:0) -- (-45:1);
\draw (0, 2)++(-55:0.8) arc [radius=0.8, start angle=-55, end angle=-5];
\end{scope}
\draw[thin] (0, 2) -- (3.5, 0);
\draw[thick, brown!60!black] (1, -3) -- (7, -3);
\draw[thin, color=blue] (3.5, 0) -- (5, -3) node [shape=circle, draw, inner sep=0pt, minimum size=1mm] {};

\draw[thick, densely dotted, brown] (1, -1.67) -- (7, -1.67);
\draw[thick, color=black!50, densely dotted] (3.5, 0) -- (6.4225, -1.67) node [shape=circle, draw, inner sep=0pt, minimum size=1mm] {};

\draw [decorate,decoration={brace,amplitude=10pt, mirror},xshift=-4pt,yshift=0pt]
(1, 0) -- (1, -3) node [midway, anchor=east, xshift=-0.6cm] {\footnotesize Actual Depth};
\draw [decorate,decoration={brace,amplitude=10pt},xshift=4pt,yshift=0pt]
(7, 0) -- (7, -1.67) node [midway, anchor=west, xshift=0.6cm] {\footnotesize Perceived Depth};
\end{tikzpicture}
\end{frame}

\begin{frame}[noframenumbering]
\frametitle{Distortion due to Refraction}
\includegraphics[height=0.8\textheight]{../thesis_template/images/Optical_refraction_at_water_surface}
\footnotetext[1]{\tiny Photography by Rainald62, licensed under CC BY-SA 3.0 -- de.wikipedia.org/wiki/Datei:Optical\_refraction\_at\_water\_surface.jpg}
\end{frame}

\begin{frame}[noframenumbering]
\frametitle{Distortion due to Refraction}
	\includegraphics[width=\textwidth]{5076124153_38341c9082_b.jpg}
	\footnotetext[1]{\tiny Photography by postbear, licensed under CC BY-NC-SA 2.0 -- www.flickr.com/photos/postbear/5076124153}
\end{frame}


\section{Previous Work}
\renewcommand{\zwischentitel}{Previous Work}
\begin{frame}
  \frametitle{Statistical Approaches}
  \begin{itemize}
  	\item<+-> Distortion modeled as a pixel shift map
  	\item<+-> Require a series of images
  	\item<+-> Analyze correlations between the images to estimate the parameters
  	\item<+-> Limited to relatively low distortion
  	\item<+-> Often can't use additional information
  \end{itemize}
\end{frame}

\begin{frame}
  \frametitle{Stereo Imaging based Approaches}
  \begin{itemize}
  	\item<+-> Require two images of the scene from different perspectives
  	\item<+-> Currently limited to planar surfaces
  	\item<+-> Use spatial information to reconstruct scene parameters, e.g. depth
  \end{itemize}
\end{frame}


\section{Image Undistortion System}
\renewcommand{\zwischentitel}{Image Undistortion System}
\begin{frame}
  \frametitle{Setup}
  %sketch of camera, surface, ground
  \begin{tikzpicture}[overlay]
	\draw (2.5, 2) -- (3, 2.5) -- (3.5, 2); %aperture
	\draw[very thin] (2.5, 2) -- (3.5, 2); %proj. plane
	\draw (2.5, 3) -- (2.5, 2.5) node[anchor=east] {Camera} -- (3.5, 2.5) -- (3.5, 3) -- cycle; %camera case
	
	%camera pixels
	\foreach \i in {0,...,4} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {5,...,8} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=green!70!blue, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {9,...,10} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=black, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {11,...,11} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=yellow, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {12,...,15} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	
	\draw[color=blue] (0, 0) node[anchor=east] {Water} sin (1, 0.5) cos (2, 0) sin (3, -0.5) cos (4, 0) sin (5, 0.5) cos (6, 0) sin (7, -0.5) cos (8, 0) sin (9, 0.5); %water
	%\draw[very thin] (0,0) -- (9, 0); %hilfslinie
	\draw[thin, color=gray!60] (0, -2) node[anchor=east] {Ground}; %ground
	
	%ground pixels
	\foreach \i in {0,...,143} {
		\node at (0.0625/2 + 0.0625*\i, -2) [rectangle, draw=black, ultra thin, inner sep=0pt, minimum size=0.0625cm] {};
	}
	
  \end{tikzpicture}
\end{frame}

\begin{frame}
\frametitle{Path Construction - Camera}
%Zoomed in on camera - animation of initial ray construction

\begin{columns}
\column[T]{0.5\textwidth}
\begin{tikzpicture}[]
	\draw (0, -2) -- (2, 0) -- (4, -2); %aperture
	\draw[very thin] (0, -2) -- (4, -2); %proj. plane
	\draw (0, 2) -- (0, 0) -- (4, 0) -- (4, 2) -- cycle; %camera case
	
	\draw [decorate,decoration={brace,amplitude=1pt},xshift=0pt,yshift=2pt, visible on=<2>]
	(0, 2.08) -- (0.25, 2.08) node [midway, anchor=south, xshift=0pt] {\footnotesize $s$};
	
	%camera pixels
	\foreach \i in {0,...,4} {
		\node at (0.125 + 0.25*\i, 2) [rectangle, draw=black, fill=brown, thin, inner sep=0pt,minimum size=0.25cm] {\textcolor{white}{\tiny \i}};
	}
	\node at (0.125 + 0.25*5, 2) [rectangle, draw=black, fill=green!70!blue, background default aspect=thin, background aspect=thick, aspect on=<2->, inner sep=0pt,minimum size=0.25cm] {\textcolor{white}{\tiny 5}};
	\foreach \i in {6,...,8} {
		\node at (0.125 + 0.25*\i, 2) [rectangle, draw=black, fill=green!70!blue, thin, inner sep=0pt,minimum size=0.25cm] {\textcolor{white}{\tiny \i}};
	}
	\foreach \i in {9,...,10} {
		\node at (0.125 + 0.25*\i, 2) [rectangle, draw=black, fill=black, thin, inner sep=0pt,minimum size=0.25cm] {\textcolor{white}{\tiny \i}};
	}
	\foreach \i in {11,...,11} {
		\node at (0.125 + 0.25*\i, 2) [rectangle, draw=black, fill=yellow, thin, inner sep=0pt,minimum size=0.25cm] {\textcolor{black}{\tiny \i}};
	}
	\foreach \i in {12,...,15} {
		\node at (0.125 + 0.25*\i, 2) [rectangle, draw=black, fill=brown, thin, inner sep=0pt,minimum size=0.25cm] {\textcolor{white}{\tiny \i}};
	}
	\node at (0.125, 2) [anchor=east, xshift=-2pt, yshift=0.5pt, visible on=<2>] {\scriptsize $i = $};
	
	\draw[background default aspect=thin, background aspect=thick, aspect on=<3>, visible on=<3->, <-, color=red] (0, 0) node[anchor=north] {$\vec{r}$} -- (2, 0);
	\draw[background default aspect=thin, background aspect=thick, aspect on=<3>, visible on=<3->, ->, color=blue] (2, 0) -- (2, -2) node [anchor=north] {$\vec{n}$};
	
	\draw[visible on=<4->, thick, densely dotted] (1.4, 2) -- (2, 0);
	\draw[visible on=<4->, thick, ->] (2, 0) -- (2.574696, -1.91565) node [anchor=south west] {$\vec{\omega_i}$};
\end{tikzpicture}

\column[T]{0.5\textwidth}
\begin{itemize}
	\item<1|only@1> World space pixel position $ p_i = (i - \frac{a}{2}) \cdot s$
	\item<2|only@2-> World space pixel position $ p_5 = (5 - \frac{15}{2}) \cdot s$
	\item<3|only@3-> Ray direction $ \vec{\omega_i} = \vec{n} - \left(p_i \cdot \begin{pmatrix}1 \\0\end{pmatrix}\right) \cdot \vec{r} $
	\item<4|only@4-> Ray origin: Pinhole
	\item<5|only@5> Initial throughput $\tau_0 = 1$
\end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\begin{tikzpicture}[overlay]
	\draw (2.5, 2) -- (3, 2.5) -- (3.5, 2); %aperture
	\draw[very thin] (2.5, 2) -- (3.5, 2); %proj. plane
	\draw (2.5, 3) -- (2.5, 2.5) node[anchor=east] {Camera} -- (3.5, 2.5) -- (3.5, 3) -- cycle; %camera case
	
	%camera pixels
	\foreach \i in {0,...,4} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {5,...,8} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=green!70!blue, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {9,...,10} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=black, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {11,...,11} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=yellow, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {12,...,15} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	
	\draw[color=blue] (0, 0) node[anchor=east] {Water} sin (1, 0.5) cos (2, 0) sin (3, -0.5) cos (4, 0) sin (5, 0.5) cos (6, 0) sin (7, -0.5) cos (8, 0) sin (9, 0.5); %water
	%\draw[very thin] (0,0) -- (9, 0); %hilfslinie
	\draw[thin, color=gray!60] (0, -2) node[anchor=east] {Ground}; %ground
	
	%ground pixels
	\foreach \i in {0,...,143} {
		\node at (0.0625/2 + 0.0625*\i, -2) [rectangle, draw=black, ultra thin, inner sep=0pt, minimum size=0.0625cm] {};
	}

	\draw[->, thick] (3, 2.5) -- (4, 0);
\end{tikzpicture}
\end{frame}

\begin{frame}[noframenumbering]
\frametitle{Path Construction - Interface Intersection}
\begin{columns}
	\column[T]{0.4\textwidth}
	\begin{tikzpicture}[]
	%\draw[very thin] (0,0) -- (1.5*pi, 0); %hilfslinie
	
	\draw[color=blue] (0, 0) sin (0.5*pi, -1) cos (pi, 0) sin (1.5*pi, 1); % water
	
	\draw[thick, ->] (pi - 1, 2.5) node[anchor=south] {$\vec{i}$} -- (pi, 0);
	
	\end{tikzpicture}
	\column[T]{0.6\textwidth}
	\begin{itemize}
		\item<1-> Reflection or transmission is randomly chosen depending on reflectance $R$ and transmittance $T$
		\item<2-> $p(\text{Reflection}) = R$, $p(\text{Transmission}) = T$
		\item<3> $\tau_{i+1} = \tau_i * w_i$ \\
		where $w_i = R$ or $T$ respectively 
	\end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Path Construction - Interface Intersection/Reflection}
%Zoom to surface, animation of an intersection - reflected ray is chosen randomly \\
%Reduced opacity to show throughput $< 1$
\begin{columns}
	\column[T]{0.4\textwidth}
	\begin{tikzpicture}[]
		%\draw[very thin] (0,0) -- (1.5*pi, 0); %hilfslinie
		
		\draw[color=blue] (0, 0) sin (0.5*pi, -1) node[anchor=south, color=black, visible on=<3->] {\small $n_i$} node [anchor=north, visible on=<3->] {\small $n_t$} cos (pi, 0) sin (1.5*pi, 1); % water
		
		\draw[thick, ->] (pi - 1, 2.5) node[anchor=south] {$\vec{i}$} -- (pi, 0);
		
		\draw[visible on=<1->] (pi - 0.5, 1.25) arc (112:135:1.3462) node[midway, above, sloped, yshift=-3pt] {\scriptsize $\theta_i$};
		\draw[very thin, color=blue!70, ->, visible on=<1->] (pi + 1, -1) -- (pi - 2, 2) node[anchor=north east] {\scriptsize $\vec{n}$};
		
		\draw[visible on=<2->] (pi - 1, 1) arc (135:158:1.4142) node[midway, above, sloped, yshift=-3pt] {\scriptsize $\theta_r$};
		\draw[thick, ->, visible on=<2->, background default aspect={color=black}, background aspect={color=black!50!white}, aspect on=<3->] (pi, 0) -- (pi - 2.5, 1) node[anchor=east] {\small $\vec{r}$};
	\end{tikzpicture}
	\column[T]{0.6\textwidth}
	\begin{itemize}
		\item<1-2|only@1-2> Law of Reflection: $$\theta_i = \theta_r$$
		\item<2|only@2> Equivalent to the Householder transformation 
		\begin{align*}
			\vec{r} &= H \vec{i} \\
			&= (I - 2\vec{n}\vec{n}^\text{T})\vec{i} \\
			&= \vec{i} - 2\vec{n}\vec{n}^\text{T}\vec{i}
		\end{align*}
		\item<3-|only@3-> Reflectance $R \in [0, 1]$ is given by the Fresnel equations
		\begin{align*}
			n_r &= \frac{n_i}{n_t} \\
			R_\text{s} &= \left|\frac{n_r\cos(\theta_i) - \cos(\theta_t)}{n_r\cos(\theta_i) + \cos(\theta_t)}\right|^2 \\
			R_\text{p} &= \left|\frac{n_r\cos(\theta_t) - \cos(\theta_i)}{n_r\cos(\theta_t) + \cos(\theta_i)}\right|^2 \\
			R &= \frac{R_\text{s} + R_\text{p}}{2}
		\end{align*}
	\end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Path Construction - Interface Intersection/Refraction}
%Animation of another intersection, this time a refraction
\begin{columns}
	\column[T]{0.4\textwidth}
	\begin{tikzpicture}[]
	%\draw[very thin] (0,0) -- (1.5*pi, 0); %hilfslinie
	
	\draw[color=blue] (0, 0) sin (0.5*pi, -1) node[anchor=south, color=black, visible on=<2->] {\small $n_i$} node [anchor=north, visible on=<2->] {\small $n_t$} cos (pi, 0) sin (1.5*pi, 1); % water
	
	\draw[thick, ->] (pi - 1, 2.5) node[anchor=south] {$\vec{i}$} -- (pi, 0); %i
	
	\draw[visible on=<1->] (pi - 0.5, 1.25) arc (112:135:1.3462) node[midway, above, sloped, yshift=-3pt] {\scriptsize $\theta_i$};
	\draw[very thin, color=blue!70, ->, visible on=<1->] (pi + 2, -2) -- (pi - 2, 2) node[anchor=north east] {\scriptsize $\vec{n}$}; %n
	
	\draw[thick, ->, visible on=<3->, background default aspect={color=black}, background aspect={color=black!50!white}, aspect on=<5->] (pi, 0) -- +(1.6, -2.1) node[anchor=north] {\small $\vec{t}$};
	\draw[visible on=<3->] (pi, 0) ++(307:1.7) arc (307:315:1.7) node[midway, anchor=north, sloped, xshift=1pt] {\scriptsize $\theta_t$};
	\end{tikzpicture}
	\column[T]{0.6\textwidth}
	\begin{itemize}
		\item<2-3|only@2-3> Snell's Law
		\begin{align*}
			n_i\cdot \sin(\theta_i) &= n_t\cdot \sin(\theta_t) \\
			\Leftrightarrow\qquad \sin(\theta_t) &= \frac{n_i}{n_t} \sin(\theta_i) 
		\end{align*}
		\item<3-4|only@3-4> Vector form:
		\begin{align*}
			\vec{t} &= n_r\cdot\vec{i} + \left(n_r \cdot(-\vec{i}\cdot\vec{n})\right. \\
			&-  \left.\sqrt{1 - n_r^2(1 - (-\vec{i}\cdot\vec{n})^2)}\right)\cdot\vec{n}
		\end{align*}
		\item<4|only@4> Imaginary root $\Rightarrow$ Total internal reflection
		\item<5-|only@5-> Transmittance
		\begin{equation*}
			T = 1 - R
		\end{equation*}
	\end{itemize}
\end{columns}
\end{frame}

\begin{frame}
\frametitle{Path Construction - Ground Intersection}
%Zoom back out, ray hits ground - path completed
\vspace{0.5cm}
\begin{tikzpicture}[overlay]
	\draw (2.5, 2) -- (3, 2.5) -- (3.5, 2); %aperture
	\draw[very thin] (2.5, 2) -- (3.5, 2); %proj. plane
	\draw (2.5, 3) -- (2.5, 2.5) node[anchor=east] {Camera} -- (3.5, 2.5) -- (3.5, 3) -- cycle; %camera case
	
	%camera pixels
	\foreach \i in {0,...,4} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {5,...,8} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=green!70!blue, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {9,...,10} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=black, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {11,...,11} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=yellow, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {12,...,15} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	
	\draw[color=blue] (0, 0) node[anchor=east] {Water} sin (1, 0.5) cos (2, 0) sin (3, -0.5) cos (4, 0) sin (5, 0.5) cos (6, 0) sin (7, -0.5) cos (8, 0) sin (9, 0.5); %water
	%\draw[very thin] (0,0) -- (9, 0); %hilfslinie
	\draw[thin, color=gray!60] (0, -2) node[anchor=east] {Ground}; %ground
	
	\draw[thick] (3, 2.5) -- (4, 0);
	\draw[->, thick, color=black!50!white] (4, 0) -- (5.4, -2);
	
	%ground pixels
	\foreach \i in {0,...,143} {
		\ifthenelse{\i = 86}{
			\node at (0.0625/2 + 0.0625*\i, -2) [rectangle, draw=black, thick, inner sep=0pt, minimum size=0.0625cm] {};
		}{
			\node at (0.0625/2 + 0.0625*\i, -2) [rectangle, draw=black, ultra thin, inner sep=0pt, minimum size=0.0625cm] {};
		}
	}
\end{tikzpicture}
\end{frame}

\begin{frame}
\frametitle{Path Evaluation}
%Color from starting point propagated along the path to the ground
\vspace{0.5cm}
\begin{tikzpicture}[overlay]
	\draw (2.5, 2) -- (3, 2.5) -- (3.5, 2); %aperture
	\draw[very thin] (2.5, 2) -- (3.5, 2); %proj. plane
	\draw (2.5, 3) -- (2.5, 2.5) node[anchor=east] {Camera} -- (3.5, 2.5) -- (3.5, 3) -- cycle; %camera case
	
	%camera pixels
	\foreach \i in {0,...,4} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\node at (2.5 + 0.0625/2 + 0.0625*5, 3) [rectangle, draw=black, fill=green!70!blue, ultra thin, inner sep=0pt,minimum size=0.0625cm, background default aspect=ultra thin, background aspect=thin, aspect on=<1->] {};
	\foreach \i in {6,...,8} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=green!70!blue, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {9,...,10} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=black, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {11,...,11} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=yellow, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	\foreach \i in {12,...,15} {
		\node at (2.5 + 0.0625/2 + 0.0625*\i, 3) [rectangle, draw=black, fill=brown, ultra thin, inner sep=0pt,minimum size=0.0625cm] {};
	}
	
	\draw[color=blue] (0, 0) node[anchor=east] {Water} sin (1, 0.5) cos (2, 0) sin (3, -0.5) cos (4, 0) sin (5, 0.5) cos (6, 0) sin (7, -0.5) cos (8, 0) sin (9, 0.5); %water
	%\draw[very thin] (0,0) -- (9, 0); %hilfslinie
	\draw[thin, color=gray!60] (0, -2) node[anchor=east] {Ground}; %ground
	
	\draw[thick, background default aspect={color=black}, background aspect={color=green!70!blue}, aspect on=<2->] (3, 2.5) -- (4, 0);
	\draw[->, thick, background default aspect={color=black!50!white}, background aspect={color=green!70!blue!50}, aspect on=<3->] (4, 0) -- (5.4, -2);
	
	%ground pixels
	\foreach \i in {0,...,143} {
		\ifthenelse{\i = 86}{
			\node at (0.0625/2 + 0.0625*\i, -2) [rectangle, draw=black, background default aspect={fill=white}, background aspect={fill=green!70!blue!50}, aspect on=<4->, ultra thin, inner sep=0pt, minimum size=0.0625cm] {};
		}{
			\node at (0.0625/2 + 0.0625*\i, -2) [rectangle, draw=black, ultra thin, inner sep=0pt, minimum size=0.0625cm] {};
		}
	}
	
\end{tikzpicture}
\end{frame}

\begin{frame}[noframenumbering]
\frametitle{Path Evaluation}
\begin{itemize}
	\item<1-> Colour of the ground pixel $\gamma$ is given by the colour of the starting pixel $c$ and the paths throughput
\end{itemize}
\pause
\begin{align*}
\gamma &= \tau_n \cdot c \\
&= \tau_0 \prod_{i=1}^{n}w_i \cdot c
\end{align*}
\end{frame}

\section{Evaluation}
\renewcommand{\zwischentitel}{Perfect Interface Structure Information}
\begin{frame}
\frametitle{Evaluation - Smooth Noise Mesh}
%maybe picture of mesh?\\
%ground truth flower $\rightarrow$ distorted flower $\rightarrow$ undistorted
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/assets/flower"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/assets/renders/smooth"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/evaluation/samples/smooth/0 - 0/1497275860-fwd-960x720-64-1-1_3"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/evaluation/samples/smooth/0 - 0/1497275867-bwd-960x720-64-1-1_3"}
\begin{onlyenv}<+->
	\begin{columns}
		\column[T]{0.5\textwidth}
		\includegraphics[width=\textwidth]{"../look_into_the_deep/evaluation/samples/smooth/0 - 0/1497275867-bwd-960x720-64-1-1_3"}
		\column[T]{0.6\textwidth}
		\begin{itemize}
			\item<.-> Close to ground truth
			\item<+-> Visible aliasing in some regions
			\item<+-> Mean Squared Error: 24.8795
			\item<+-> Structural Similarity Index Measure: 94.5\%
		\end{itemize}
	\end{columns}
\end{onlyenv}
\end{frame}

\begin{frame}
\frametitle{Evaluation - Noise Mesh}
%maybe picture of mesh?\\
%ground truth text $\rightarrow$ distorted text $\rightarrow$ undistorted
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/assets/text"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/assets/renders/noise"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/evaluation/samples/noise - noise/linear/1497273810-fwd-1277x928-64-1-1_3"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/evaluation/samples/noise - noise/linear/1497273826-bwd-1277x928-64-1-1_3"}
\begin{onlyenv}<+->
	\begin{columns}
		\column[T]{0.5\textwidth}
		\includegraphics[width=\textwidth]{"../look_into_the_deep/evaluation/samples/noise - noise/linear/1497273826-bwd-1277x928-64-1-1_3"}
		\column[T]{0.6\textwidth}
		\begin{itemize}
			\item<.-> Complete loss of fine details
			\item<+-> Recognizable as text
			\item<+-> Mean Squared Error: 137.346
			\item<+-> Structural Similarity Index Measure: 56.2\%
		\end{itemize}
	\end{columns}
\end{onlyenv}
\end{frame}

\begin{frame}
\frametitle{Evaluation - Increased Input Resolution \only<1>{1x}\only<2>{2x}\only<3>{3x}}
%undistortion from different resolutions
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/evaluation/samples/noise - noise/linear/1497273826-bwd-1277x928-64-1-1_3"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/evaluation/samples/noise - noise/2x resolution/1497274193-bwd-2554x1856-64-1-1_3"}
\includegraphics<+>[width=0.9\textwidth]{"../look_into_the_deep/evaluation/samples/noise - noise/3x resolution/1497274755-bwd-3831x2784-64-1-1_3"}
\begin{onlyenv}<+->
	\begin{columns}
		\column[T]{0.5\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			ylabel={\scriptsize Mean Squared Error},
			ylabel style={color=pcolor1},
			legend style={color=pcolor1},
			tick label style={font=\tiny},
			label style={font=\tiny},
			width=\textwidth,
			ymin=0,
			xlabel={\scriptsize Resolution Factor},
			axis y line*=left,
			]
			\addplot[mark=*, pcolor1] coordinates {
				(1, 137.346)
				(2, 95.5422)
				(3, 78.4172)
			};
			\end{axis}
			\begin{axis}[
			ylabel={\scriptsize SSIM},
			ylabel style={color=pcolor2},
			legend style={color=pcolor2},
			tick label style={font=\tiny},
			label style={font=\tiny},
			width=\textwidth,
			ymin=0,
			ymax=1,
			xlabel={\scriptsize Resolution Factor},
			axis y line*=right,
			axis x line=none,
			ymajorgrids,
			]
			\addplot[mark=+, pcolor2] coordinates {
				(1, 0.562003)
				(2, 0.725876)
				(3, 0.810086)
			};
			\end{axis}
		\end{tikzpicture}
		\column[T]{0.5\textwidth}
		\begin{itemize}
			\item<.-> Large influence on sharpness
			\item<+-> MSE reduced by 43\%
			\item<+-> SSIM increased by 25 points
			\item<+-> $\Rightarrow$ Input resolution is very important
		\end{itemize}
	\end{columns}
\end{onlyenv}
\end{frame}

\renewcommand{\zwischentitel}{Limited Interface Structure Information}
\begin{frame}
\frametitle{Evaluation - Subsampling \only<4>{0x}\only<5>{1x}\only<6>{2x}\only<7>{3x}\only<8>{4x}}
%maybe picture of mesh?\\
%undistorted images; graph
\begin{onlyenv}<1-3>
	\begin{itemize}
		\item<+-> Mesh resembles an ocean wave
		\item<+-> \emph{Un-Subdivided} to simulate the imprecise capture of surface data
		\item<+-> each iteration approximately halves the number of triangles
	\end{itemize}
\end{onlyenv}
\makebox[\textwidth]{%
	\includegraphics<+>[width=0.5\paperwidth]{"../look_into_the_deep/assets/renders/ss0"}%
	\includegraphics<.>[width=0.5\paperwidth]{"../look_into_the_deep/evaluation/samples/wave subsampling/0 - 0/1497271922-bwd-1277x928-64-1-1_3"}%
	\includegraphics<+>[width=0.5\paperwidth]{"../look_into_the_deep/assets/renders/ss1"}%
	\includegraphics<.>[width=0.5\paperwidth]{"../look_into_the_deep/evaluation/samples/wave subsampling/0 - 1/1497272170-bwd-1277x928-64-1-1_3"}%
	\includegraphics<+>[width=0.5\paperwidth]{"../look_into_the_deep/assets/renders/ss2"}%
	\includegraphics<.>[width=0.5\paperwidth]{"../look_into_the_deep/evaluation/samples/wave subsampling/0 - 2/1497272449-bwd-1277x928-64-1-1_3"}%
	\includegraphics<+>[width=0.5\paperwidth]{"../look_into_the_deep/assets/renders/ss3"}%
	\includegraphics<.>[width=0.5\paperwidth]{"../look_into_the_deep/evaluation/samples/wave subsampling/0 - 3/1497272607-bwd-1277x928-64-1-1_3"}%
	\includegraphics<+>[width=0.5\paperwidth]{"../look_into_the_deep/assets/renders/ss4"}%
	\includegraphics<.>[width=0.5\paperwidth]{"../look_into_the_deep/evaluation/samples/wave subsampling/0 - 4/1497442441-bwd-1277x928-64-1-1_3"}%
	\hspace{0.6cm}
}

\begin{onlyenv}<+->
	\begin{columns}
		\column[T]{0.5\textwidth}
		\begin{tikzpicture}
			\begin{axis}[
			ylabel={\scriptsize Mean Squared Error},
			ylabel style={color=pcolor1},
			tick label style={font=\tiny},
			label style={font=\tiny},
			width=\textwidth,
			legend style={at={(0.5,-0.15)},anchor=north,legend columns=-1},
			ymin=0,
			xlabel={\scriptsize Subsampling Exponent},
			axis y line*=left,
			]
			\addplot[mark=x, pcolor1] coordinates {
				( 0, 36.538)
				( 1, 40.6827)
				( 2, 44.3575)
				( 3, 46.7527)
				( 4, 49.5371)
			};
			\end{axis}
			\begin{axis}[
			ylabel={\scriptsize SSIM},
			ylabel style={color=pcolor2},
			tick label style={font=\tiny},
			label style={font=\tiny},
			width=\textwidth,
			ymin=0,
			ymax=1,
			axis y line*=right,
			axis x line=none,
			ymajorgrids,
			]
			\addplot[mark=x, pcolor2] coordinates {
				( 0, 0.97517)
				( 1, 0.7319)
				( 2, 0.576599)
				( 3, 0.486779)
				( 4, 0.405798)
			};
			\end{axis}
		\end{tikzpicture}
		\column[T]{0.5\textwidth}
		\begin{itemize}
			\item<.-> Undistortion quality falls off quickly
			\item<+-> Many artifacts after only one iteration
			\item<+-> $\Rightarrow$ High dependence on input information
		\end{itemize}
	\end{columns}
\end{onlyenv}
\end{frame}


\section{Conclusion}
\renewcommand{\zwischentitel}{Conclusion}
\begin{frame}
\frametitle{Strengths}
\begin{itemize}
	\item<+-> High performance with precise information
	\item<+-> Even extremely distorted images can be restored
	\item<+-> High extensibility
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Weaknesses}
\begin{itemize}
	\item<+-> Highly dependent on input
	\item<+-> Vulnerable to undersampling
	\item<+-> Geometry must be precisely known
\end{itemize}
\end{frame}

\begin{frame}
\frametitle{Possible Improvements and Extensions}
\begin{itemize}
	\item<+-> Pre-/post-processing
	\item<+-> Simulation of additional physical effects
	\item<+-> Path tracing improvements
	\item<+-> Integration of parameter estimation methods
\end{itemize}
\end{frame}
%use reflection of sun/sky to estimate surface orientation

\section{References}
\renewcommand{\zwischentitel}{References}
\begin{frame}
\frametitle{References}
\nocite{tian2009seeing}
\nocite{4543331}
\nocite{veach1998robust}
\bibliographystyle{IEEEtranS}
\bibliography{../thesis_template/bibliography.bib}
\end{frame}

\end{document}