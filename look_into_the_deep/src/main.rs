#![allow(unknown_lints)]
#![allow(useless_format)]

#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate glium;
#[macro_use]
extern crate log;
extern crate simplelog;
extern crate log_panics;
extern crate cgmath;
extern crate num;
extern crate image;
extern crate itertools;
extern crate time;
extern crate rand;


#[macro_use]
mod macros;
mod renderer;
mod pathtracer;
mod tray_rust;
mod config;

use std::path::PathBuf;
use std::fmt::Debug;
use pathtracer::{RenderDirection, Image};
use num::Float;

lazy_static!{
    pub static ref CONFIG: config::Config =
        config::Config::from_file("./assets/");
}

fn main() {
    log_panics::init();
    use simplelog::{Config, TermLogger, WriteLogger, CombinedLogger, LogLevelFilter};
    CombinedLogger::init(vec![TermLogger::new(LogLevelFilter::Info, Config::default())
                                  .expect("could not init TermLogger"),
                              WriteLogger::new(LogLevelFilter::Debug,
                                               Config::default(),
                                               ::std::fs::File::create("debug.log")
                                                   .expect("could not init FileLogger"))])
        .expect("could not create Logger");

    if CONFIG.renderer.enabled {
        let mut renderer = renderer::Renderer::new("Look into the Deep".to_owned(),
                                                   (0.0, 0.0, 0.0, 1.0));
        renderer.start();
    } else {
        let cameraparams = pathtracer::CameraParams {
            pos: CONFIG.camera.position,
            center: CONFIG.camera.center,
            up: CONFIG.camera.up,
            fov: CONFIG.camera.fov,
        };
        match CONFIG.direction {
            Some(RenderDirection::Forward) => {
                fwd(&cameraparams, CONFIG.intermediate.drop_alpha);
            }
            Some(RenderDirection::Backward) => {
                bwd(&cameraparams, &CONFIG.input.image, CONFIG.output.drop_alpha);
            }
            None => {
                let intermediate = fwd(&cameraparams, CONFIG.intermediate.drop_alpha);
                bwd(&cameraparams, &intermediate, CONFIG.output.drop_alpha);
            }
        }



    }
}

fn fwd(cameraparams: &pathtracer::CameraParams, drop_alpha: bool) -> Image<f32> {
    let fwd_dim = (CONFIG.intermediate.width,
                   (CONFIG.intermediate.width as f64 / CONFIG.input.image.width() as f64 *
                    CONFIG.input.image.height() as f64) as usize);
    let image_fwd = pathtracer::raytrace(&CONFIG.pathtracer,
                                         cameraparams,
                                         &CONFIG.input.image,
                                         &CONFIG.medium.meshcfg,
                                         RenderDirection::Forward,
                                         fwd_dim,
                                         CONFIG.medium.n_outside);
    image_fwd.store(&str_to_path(
        &format!("./out/{t}-fwd-{from_x}x{from_y}-{spp}-{n_outside}-{n_inside}.png",
                                          t = time::now().to_timespec().sec,
                                          from_x = CONFIG.input.image.get_dimensions().0,
                                          from_y = CONFIG.input.image.get_dimensions().1,
                                          spp = CONFIG.pathtracer.spp,
                                          n_outside = CONFIG.medium.n_outside,
                                          n_inside = CONFIG.medium.n_inside)),
                    drop_alpha);
    image_fwd
}

fn bwd<T>(cameraparams: &pathtracer::CameraParams, input: &Image<T>, drop_alpha: bool)
    where T: Float + Send + Sync + From<f32> + Debug
{
    let fwd_dim = input.get_dimensions();
    let bwd_dim = (CONFIG.output.width,
                   (CONFIG.output.width as f64 / CONFIG.input.image.width() as f64 *
                    CONFIG.input.image.height() as f64) as usize);
    let image_bwd = pathtracer::raytrace(&CONFIG.pathtracer,
                                         cameraparams,
                                         input,
                                         &CONFIG.medium.meshcfg,
                                         RenderDirection::Backward,
                                         bwd_dim,
                                         CONFIG.medium.n_outside);
    image_bwd.store(&str_to_path(
        &format!("./out/{t}-bwd-{from_x}x{from_y}-{spp}-{n_outside}-{n_inside}.png",
                                        t = time::now().to_timespec().sec,
                                        from_x = fwd_dim.0,
                                        from_y = fwd_dim.1,
                                        spp = CONFIG.pathtracer.spp,
                                        n_outside = CONFIG.medium.n_outside,
                                        n_inside = CONFIG.medium.n_inside)),
                    drop_alpha);
}

pub fn str_to_path(path: &str) -> PathBuf {
    use std::path::Path;
    path.split('/')
        .map(|x| Path::new(x))
        .fold(PathBuf::new(), |mut acc, x| {
            acc.push(x);
            acc
        })
}
