extern crate config;

use pathtracer::geometry::Mesh;
use pathtracer::{Image, Color};
use pathtracer::direction::Interpolation;
use pathtracer::RenderDirection;
use super::image;
use itertools::Itertools;
use cgmath::{Point3, Vector3};

pub struct Config {
    pub medium: MediumConfig,
    pub input: InputConfig,
    pub intermediate: IntermediateConfig,
    pub output: OutputConfig,
    pub camera: CameraConfig,
    pub direction: Option<RenderDirection>,
    pub renderer: RendererConfig,
    pub pathtracer: PathtracerConfig,
}

pub struct MeshConfig {
    pub meshes: Vec<Mesh>,
    pub idx_fwd: usize,
    pub idx_bwd: usize,
}

pub struct MediumConfig {
    pub meshcfg: MeshConfig,
    pub n_inside: f32,
    pub n_outside: f32,
}

pub struct InputConfig {
    pub image: Image<f32>,
}

pub struct IntermediateConfig {
    pub width: usize,
    pub drop_alpha: bool,
}
pub struct OutputConfig {
    pub width: usize,
    pub drop_alpha: bool,
}

pub struct CameraConfig {
    pub position: Point3<f32>,
    pub center: Point3<f32>,
    pub up: Vector3<f32>,
    pub fov: f32,
}

pub struct RendererConfig {
    pub enabled: bool,
    pub width: u32,
    pub height: u32,
}

pub struct PathtracerConfig {
    pub tilewidth: usize,
    pub tileheight: usize,
    pub threads: usize,
    pub buffersize: usize,
    pub spp: usize,
    pub max_iterations: u32,
    pub interpolation: Interpolation,
    pub background: Color<f32>,
}


impl Config {
    pub fn from_file(path: &str) -> Self {
        use super::str_to_path;

        let mut config = config::Config::new();
        config
            .merge(config::File::new("config.toml", config::FileFormat::Toml).path(path))
            .expect("could not load config");
        let meshnames_fwd: Vec<String> = {
            config
                .get_array("medium.meshnames_fwd")
                .and_then(|v| v.into_iter().map(config::Value::into_str).collect())
                .expect("could not find meshnames_fwd in config")
        };
        let meshnames_bwd: Vec<String> = {
            config
                .get_array("medium.meshnames_bwd")
                .and_then(|v| v.into_iter().map(config::Value::into_str).collect())
                .expect("could not find meshnames_bwd in config")
        };
        let (meshes, bwd, fwd) =
            Mesh::load_obj(&str_to_path(&config
                                            .get_str("medium.mesh")
                                            .expect("must specify mesh in config")),
                           config.get_float("medium.n_inside").unwrap_or(1.3) as f32,
                           config.get_float("medium.n_outside").unwrap_or(1.0) as f32,
                           meshnames_fwd,
                           meshnames_bwd);
        assert!(!meshes.is_empty());
        debug!("meshes length: {:?}", meshes.len());
        let medium = MediumConfig {
            meshcfg: MeshConfig {
                meshes: meshes,
                idx_fwd: fwd,
                idx_bwd: bwd,
            },
            n_inside: config.get_float("medium.n_inside").unwrap_or(2.0) as f32,
            n_outside: config.get_float("medium.n_outside").unwrap_or(1.0) as f32,
        };

        let image = {
            let image = image::open(&str_to_path(&config
                                                     .get_str("input.image")
                                                     .expect("must specify input image")))
                .expect("could not load image")
                .to_rgba();
            let image = image::imageops::flip_vertical(&image);
            let dimensions = image.dimensions();
            let image: Vec<_> = image
                .into_raw()
                .iter()
                .chunks(4)
                .into_iter()
                .map(|mut it| {
                         Color {
                             r: *it.next().unwrap() as f32 / 255.0,
                             g: *it.next().unwrap() as f32 / 255.0,
                             b: *it.next().unwrap() as f32 / 255.0,
                             a: *it.next().unwrap() as f32 / 255.0,
                         }
                     })
                .collect();
            let image = Image::from_buffer(dimensions.0 as usize, dimensions.1 as usize, image);
            assert_eq!(dimensions.0 * dimensions.1, image.data().len() as u32);
            info!("'from'-image dim {:?}", image.get_dimensions());
            image
        };
        let input = InputConfig { image: image };

        let intermediate = IntermediateConfig {
            width: config
                .get_int("intermediate.scaling_factor")
                .map(|factor| input.image.get_dimensions().0 as i64 * factor)
                .or_else(|| config.get_int("intermediate.width"))
                .unwrap_or(1024) as usize,
            drop_alpha: config.get_bool("intermediate.drop_alpha").unwrap_or(false),
        };

        let output = OutputConfig {
            width: config
                .get_int("output.scaling_factor")
                .map(|factor| input.image.get_dimensions().0 as i64 * factor)
                .or_else(|| config.get_int("output.width"))
                .unwrap_or(1024) as usize,
            drop_alpha: config.get_bool("output.drop_alpha").unwrap_or(false),
        };

        let position = Point3::new(config.get_float("camera.position.x").unwrap_or(0.0) as f32,
                                   config.get_float("camera.position.y").unwrap_or(5.0) as f32,
                                   config.get_float("camera.position.z").unwrap_or(0.0) as f32);
        let center = Point3::new(config.get_float("camera.center.x").unwrap_or(0.0) as f32,
                                 config.get_float("camera.center.y").unwrap_or(0.0) as f32,
                                 config.get_float("camera.center.z").unwrap_or(0.0) as f32);
        let up = Vector3::new(config.get_float("camera.up.x").unwrap_or(1.0) as f32,
                              config.get_float("camera.up.y").unwrap_or(0.0) as f32,
                              config.get_float("camera.up.z").unwrap_or(0.0) as f32);
        let camera = CameraConfig {
            position: position,
            center: center,
            up: up,
            fov: config.get_float("camera.fov").unwrap_or(75.0) as f32,
        };

        let renderer = RendererConfig {
            enabled: config.get_bool("renderer.enabled").unwrap_or(false),
            width: config.get_int("renderer.width").unwrap_or(1920) as u32,
            height: config.get_int("renderer.height").unwrap_or(1080) as u32,
        };

        let pathtracer = PathtracerConfig {
            tilewidth: config.get_int("pathtracer.tilewidth").unwrap_or(1) as usize,
            tileheight: config.get_int("pathtracer.tileheight").unwrap_or(1) as usize,
            threads: config.get_int("pathtracer.threads").unwrap_or(0) as usize,
            buffersize: config.get_int("pathtracer.buffersize_MB").unwrap_or(2048) as usize,
            spp: config.get_int("pathtracer.spp").unwrap_or(1) as usize,
            max_iterations: config.get_int("pathtracer.max_iterations").unwrap_or(100) as u32,
            interpolation: config
                .get_str("pathtracer.interpolation")
                .and_then(|s| match &*s {
                              "nearest" => Some(Interpolation::Nearest),
                              "linear" => Some(Interpolation::Linear),
                              _ => None,
                          })
                .expect("interpolation must be set to 'nearest' or 'linear'"),
            background: config
                .get_array("pathtracer.background")
                .and_then(|vec| {
                              vec.into_iter()
                                  .map(config::Value::into_float)
                                  .collect::<Option<_>>()
                          })
                .and_then(|vec: Vec<_>| if vec.len() != 4 {
                              None
                          } else {
                              Some(Color {
                                       r: vec[0] as f32,
                                       g: vec[1] as f32,
                                       b: vec[2] as f32,
                                       a: vec[3] as f32,
                                   })
                          })
                .unwrap_or_else(Color::zero),
        };

        let direction =
            config.get_str("direction").map(|s| match &*s {
                                                "fwd" => RenderDirection::Forward,
                                                "bwd" => RenderDirection::Backward,
                                                d => {
                                                    panic!("invalid direction specified: \"{:?}\"",
                                                           d)
                                                }
                                            });

        Config {
            medium: medium,
            input: input,
            intermediate: intermediate,
            output: output,
            camera: camera,
            direction: direction,
            renderer: renderer,
            pathtracer: pathtracer,
        }
    }
}
