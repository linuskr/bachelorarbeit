extern crate glium;

use pathtracer::{self, Image};

use glium::texture::{Texture2d, DepthTexture2d};
use glium::framebuffer::SimpleFrameBuffer;
use glium::{VertexBuffer, Program, DrawParameters, Vertex};
use cgmath::{Matrix4, Vector3, vec3, Point3, PerspectiveFov, Deg, Rad};
use cgmath::prelude::*;

#[derive(Copy, Clone)]
struct Vertex2D {
    position: [f32; 2],
    tex_coord: [f32; 2],
    tex_id: u32,
}

#[derive(Copy, Clone)]
pub struct Vertex3D {
    pub position: [f32; 3],
    pub normal: [f32; 3], // n = (0,0,0) => ground
    pub tex_coord: [f32; 2],
}

implement_vertex!(Vertex2D, position, tex_coord, tex_id);
implement_vertex!(Vertex3D, position, normal, tex_coord);

struct Renderable<'a, T: Vertex> {
    pub vb: VertexBuffer<T>,
    pub ib: glium::index::NoIndices,
    pub program: Program,
    pub params: DrawParameters<'a>,
}

impl<'a, T: Vertex> Renderable<'a, T> {
    fn new(vb: VertexBuffer<T>,
           ib: glium::index::NoIndices,
           program: Program,
           params: DrawParameters<'a>)
           -> Self {
        Renderable {
            vb: vb,
            ib: ib,
            program: program,
            params: params,
        }
    }
}

pub struct Renderer<'a> {
    display: glium::backend::glutin_backend::GlutinFacade,
    composite: Renderable<'a, Vertex2D>,
    scene_transparents: Renderable<'a, Vertex3D>,
    scene: Renderable<'a, Vertex3D>,
    camera: FPSCamera,
    clear_color: (f32, f32, f32, f32),
}

impl<'a> Renderer<'a> {
    pub fn new(title: String, clear_color: (f32, f32, f32, f32)) -> Self {
        use glium::DisplayBuild;
        let config = &super::CONFIG;
        let display = glium::glutin::WindowBuilder::new()
            .with_dimensions(config.renderer.width, config.renderer.height)
            .with_title(title)
            .build_glium()
            .unwrap();

        let composite = {
            let quads = vec![// Scene
                             Vertex2D {
                                 position: [-1.0, -1.0],
                                 tex_coord: [0.0, 0.0],
                                 tex_id: 0,
                             },
                             Vertex2D {
                                 position: [0.5, -1.0],
                                 tex_coord: [1.0, 0.0],
                                 tex_id: 0,
                             },
                             Vertex2D {
                                 position: [-1.0, 1.0],
                                 tex_coord: [0.0, 1.0],
                                 tex_id: 0,
                             },
                             Vertex2D {
                                 position: [-1.0, 1.0],
                                 tex_coord: [0.0, 1.0],
                                 tex_id: 0,
                             },
                             Vertex2D {
                                 position: [0.5, -1.0],
                                 tex_coord: [1.0, 0.0],
                                 tex_id: 0,
                             },
                             Vertex2D {
                                 position: [0.5, 1.0],
                                 tex_coord: [1.0, 1.0],
                                 tex_id: 0,
                             },

                             // fwd
                             Vertex2D {
                                 position: [0.5, -1.0],
                                 tex_coord: [0.0, 0.0],
                                 tex_id: 1,
                             },
                             Vertex2D {
                                 position: [1.0, -1.0],
                                 tex_coord: [1.0, 0.0],
                                 tex_id: 1,
                             },
                             Vertex2D {
                                 position: [0.5, 0.0],
                                 tex_coord: [0.0, 1.0],
                                 tex_id: 1,
                             },
                             Vertex2D {
                                 position: [0.5, 0.0],
                                 tex_coord: [0.0, 1.0],
                                 tex_id: 1,
                             },
                             Vertex2D {
                                 position: [1.0, -1.0],
                                 tex_coord: [1.0, 0.0],
                                 tex_id: 1,
                             },
                             Vertex2D {
                                 position: [1.0, 0.0],
                                 tex_coord: [1.0, 1.0],
                                 tex_id: 1,
                             },

                             // bwd
                             Vertex2D {
                                 position: [0.5, 0.0],
                                 tex_coord: [0.0, 0.0],
                                 tex_id: 2,
                             },
                             Vertex2D {
                                 position: [1.0, 0.0],
                                 tex_coord: [1.0, 0.0],
                                 tex_id: 2,
                             },
                             Vertex2D {
                                 position: [0.5, 1.0],
                                 tex_coord: [0.0, 1.0],
                                 tex_id: 2,
                             },
                             Vertex2D {
                                 position: [0.5, 1.0],
                                 tex_coord: [0.0, 1.0],
                                 tex_id: 2,
                             },
                             Vertex2D {
                                 position: [1.0, 0.0],
                                 tex_coord: [1.0, 0.0],
                                 tex_id: 2,
                             },
                             Vertex2D {
                                 position: [1.0, 1.0],
                                 tex_coord: [1.0, 1.0],
                                 tex_id: 2,
                             }];

            let vb = VertexBuffer::new(&display, &quads).unwrap();
            let ib = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);

            let program = Program::from_source(&display,
                                               include_str!("./shader/comp_vert.glsl"),
                                               include_str!("./shader/comp_frag.glsl"),
                                               None)
                .expect("error compiling composition shaders");

            let params = DrawParameters {
                backface_culling: glium::draw_parameters::BackfaceCullingMode::CullClockwise,
                ..Default::default()
            };
            Renderable::new(vb, ib, program, params)
        };

        let scene_transparents = {
            let vb = {
                let mut vertices =
                    Vec::with_capacity(config
                                           .medium
                                           .meshcfg
                                           .meshes
                                           .iter()
                                           .filter(|m| !m.is_ground)
                                           .fold(0, |acc, v| acc + v.vertex_data.len()));
                for vec in &config.medium.meshcfg.meshes {
                    if !vec.is_ground {
                        vertices.extend_from_slice(&*vec.vertex_data)
                    }
                }
                VertexBuffer::new(&display, &*vertices).unwrap()
            };
            let ib = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);
            let program = Program::from_source(&display,
                                               include_str!("./shader/scene_vert.glsl"),
                                               include_str!("./shader/scene_frag.glsl"),
                                               None)
                .expect("error compiling scene shaders");
            let params = DrawParameters {
                backface_culling: glium::draw_parameters::BackfaceCullingMode::CullClockwise,
                blend: glium::Blend {
                    color: glium::BlendingFunction::Addition {
                        source: glium::LinearBlendingFactor::SourceAlpha,
                        destination: glium::LinearBlendingFactor::OneMinusSourceAlpha,
                    },
                    alpha: glium::BlendingFunction::Addition {
                        source: glium::LinearBlendingFactor::One,
                        destination: glium::LinearBlendingFactor::One,
                    },
                    ..Default::default()
                },
                depth: glium::Depth {
                    test: glium::DepthTest::Overwrite,
                    write: false, // not necessary now, MAY BREAK IF STH IS RENDERED LATER!
                    ..Default::default()
                },
                ..Default::default()
            };
            Renderable::new(vb, ib, program, params)
        };

        let scene = {
            let vb = {
                let mut vertices =
                    Vec::with_capacity(config
                                           .medium
                                           .meshcfg
                                           .meshes
                                           .iter()
                                           .filter(|m| m.is_ground)
                                           .fold(0, |acc, v| acc + v.vertex_data.len()));
                for vec in &config.medium.meshcfg.meshes {
                    if vec.is_ground {
                        vertices.extend_from_slice(&*vec.vertex_data)
                    }
                }
                VertexBuffer::new(&display, &*vertices).unwrap()
            };
            let ib = glium::index::NoIndices(glium::index::PrimitiveType::TrianglesList);
            let program = Program::from_source(&display,
                                               include_str!("./shader/scene_vert.glsl"),
                                               include_str!("./shader/scene_frag.glsl"),
                                               None)
                .expect("error compiling scene shaders");
            let params = DrawParameters {
                backface_culling: glium::draw_parameters::BackfaceCullingMode::CullClockwise,
                blend: glium::Blend {
                    color: glium::BlendingFunction::Addition {
                        source: glium::LinearBlendingFactor::SourceAlpha,
                        destination: glium::LinearBlendingFactor::OneMinusSourceAlpha,
                    },
                    alpha: glium::BlendingFunction::Addition {
                        source: glium::LinearBlendingFactor::One,
                        destination: glium::LinearBlendingFactor::One,
                    },
                    ..Default::default()
                },
                depth: glium::Depth {
                    test: glium::DepthTest::IfLess,
                    write: true,
                    ..Default::default()
                },
                ..Default::default()
            };
            Renderable::new(vb, ib, program, params)
        };


        Renderer {
            display: display,
            composite: composite,
            scene: scene,
            scene_transparents: scene_transparents,
            camera: FPSCamera::new(config.camera.position,
                                   config.camera.center,
                                   config.camera.up,
                                   Deg(config.camera.fov),
                                   config.renderer.width as f32 * 0.75 /
                                   config.renderer.height as f32),
            clear_color: clear_color,
        }
    }

    pub fn start(&mut self) {
        let config = &super::CONFIG;
        let scene_dim = ((config.renderer.width as f32 * 0.75) as u32,
                         config.renderer.height as u32);
        let texture_scene =
            Texture2d::empty_with_format(&self.display,
                                         glium::texture::UncompressedFloatFormat::F32F32F32F32,
                                         glium::texture::MipmapsOption::NoMipmap,
                                         scene_dim.0,
                                         scene_dim.1)
                .unwrap();
        let texture_depth =
            DepthTexture2d::empty_with_format(&self.display,
                                              glium::texture::DepthFormat::F32,
                                              glium::texture::MipmapsOption::NoMipmap,
                                              scene_dim.0,
                                              scene_dim.1)
                .unwrap();
        let mut fb_scene =
            SimpleFrameBuffer::with_depth_buffer(&self.display, &texture_scene, &texture_depth)
                .unwrap();
        let mut texture_fwd = Texture2d::new(&self.display, Image::new(1, 1).into_rawimage2d())
            .unwrap();
        let mut texture_bwd = Texture2d::new(&self.display, Image::new(1, 1).into_rawimage2d())
            .unwrap();
        let texture_ground =
            Texture2d::new(&self.display, config.input.image.clone().into_rawimage2d()).unwrap();
        let mut dt = super::time::Duration::zero();
        let inverted = Matrix4::look_at(config.camera.position,
                                        config.camera.center,
                                        config.camera.up)
            .inverse_transform()
            .unwrap();
        'main: loop {
            use std::thread::sleep;
            use std::time::Duration;
            use pathtracer::RenderDirection;
            use time::PreciseTime;

            let start = PreciseTime::now();
            for ev in self.display.poll_events() {
                use glium::glutin::{Event, ElementState, VirtualKeyCode};
                match ev {
                    Event::Closed |
                    Event::KeyboardInput(ElementState::Pressed,
                                         _,
                                         Some(VirtualKeyCode::Escape)) => break 'main,
                    Event::KeyboardInput(ElementState::Pressed, _, Some(VirtualKeyCode::F)) => {
                        use str_to_path;
                        use time; //hehe
                        let view_mat = self.camera.pathtracer_view();
                        let view_mat = inverted * view_mat;
                        let cameraparams = pathtracer::CameraParams {
                            pos: view_mat.transform_point(config.camera.position),
                            center: view_mat.transform_point(config.camera.center),
                            up: view_mat.transform_vector(config.camera.up),
                            fov: {
                                let Deg(fov) = self.camera.fov();
                                fov
                            },
                        };
                        let fwd_dim = (config.intermediate.width,
                                       (config.intermediate.width as f64 /
                                        config.input.image.width() as f64 *
                                        config.input.image.height() as f64) as
                                       usize);
                        let image_fwd = pathtracer::raytrace(&config.pathtracer,
                                                             &cameraparams,
                                                             &config.input.image,
                                                             &config.medium.meshcfg,
                                                             RenderDirection::Forward,
                                                             fwd_dim,
                                                             config.medium.n_outside);
                        image_fwd.store(&str_to_path(
                            &format!(
                                "./out/{t}-fwd-{from_x}x{from_y}-{spp}-{n_outside}-{n_inside}.png",
                                    t = time::now().to_timespec().sec,
                                    from_x = config.input.image.get_dimensions().0,
                                    from_y = config.input.image.get_dimensions().1,
                                    spp = config.pathtracer.spp,
                                    n_outside = config.medium.n_outside,
                                    n_inside = config.medium.n_inside)),
                            config.intermediate.drop_alpha);


                        let bwd_dim = (config.output.width,
                                       (config.output.width as f64 /
                                        config.input.image.width() as f64 *
                                        config.input.image.height() as f64) as
                                       usize);
                        let image_bwd = pathtracer::raytrace(&config.pathtracer,
                                                             &cameraparams,
                                                             &image_fwd,
                                                             &config.medium.meshcfg,
                                                             RenderDirection::Backward,
                                                             bwd_dim,
                                                             config.medium.n_outside);
                        image_bwd.store(&str_to_path(
                            &format!(
                                "./out/{t}-bwd-{from_x}x{from_y}-{spp}-{n_outside}-{n_inside}.png",
                                    t = time::now().to_timespec().sec,
                                    from_x = fwd_dim.0,
                                    from_y = fwd_dim.1,
                                    spp = config.pathtracer.spp,
                                    n_outside = config.medium.n_outside,
                                    n_inside = config.medium.n_inside)),
                            config.output.drop_alpha);


                        texture_fwd = Texture2d::new(&self.display, image_fwd.into_rawimage2d())
                            .unwrap();
                        texture_bwd = Texture2d::new(&self.display, image_bwd.into_rawimage2d())
                            .unwrap();
                    }
                    Event::MouseMoved(dx, dy) => self.camera.handle_mouse_move(dx, dy),
                    Event::MouseInput(state, button) => {
                        self.camera.handle_mouse_click(button, state)
                    }
                    Event::KeyboardInput(state, _, Some(keycode)) => {
                        self.camera.handle_keypress(state, keycode)
                    }

                    _ => (),
                }
            }
            self.camera.update_view(dt.num_milliseconds() as f32);
            self.draw(&texture_scene,
                      &mut fb_scene,
                      &texture_ground,
                      &texture_fwd,
                      &texture_bwd);
            sleep(Duration::from_millis(10u64.saturating_sub(start
                                                                 .to(PreciseTime::now())
                                                                 .num_milliseconds() as
                                                             u64)));
            dt = start.to(PreciseTime::now());
        }
    }

    pub fn draw(&mut self,
                texture_scene: &Texture2d,
                fb_scene: &mut SimpleFrameBuffer,
                texture_ground: &Texture2d,
                texture_fwd: &Texture2d,
                texture_bwd: &Texture2d) {
        use glium::Surface;
        let (view, projection) = self.camera.get_view_projection();

        let uniforms =
            uniform!{
            modelview_matrix: Into::<[[f32; 4]; 4]>::into(view),
            proj_matrix: Into::<[[f32; 4]; 4]>::into(projection),
            tex_ground: texture_ground,
        };
        fb_scene.clear_color_and_depth(self.clear_color, 1.0);
        fb_scene
            .draw(&self.scene.vb,
                  &self.scene.ib,
                  &self.scene.program,
                  &uniforms,
                  &self.scene.params)
            .unwrap();
        fb_scene
            .draw(&self.scene_transparents.vb,
                  &self.scene_transparents.ib,
                  &self.scene_transparents.program,
                  &uniforms,
                  &self.scene_transparents.params)
            .unwrap();


        let mut target = self.display.draw();

        target.clear_color_and_depth(self.clear_color, 1.0);
        let uniforms =
            uniform! {
            tex_scene: texture_scene,
            tex_fwd: texture_fwd,
            tex_bwd: texture_bwd,
        };
        target
            .draw(&self.composite.vb,
                  &self.composite.ib,
                  &self.composite.program,
                  &uniforms,
                  &self.composite.params)
            .unwrap();

        target.finish().unwrap();
        assert_no_gl_error!(self.display);
    }
}


struct FPSCamera {
    transform: Matrix4<f32>,
    translation: Vector3<f32>,
    rotation: (Rad<f32>, Rad<f32>),
    projection: PerspectiveFov<f32>,
    mouse_pos: (i32, i32),
    mouse_speed: (f32, f32),
    move_speed: f32,
    keystates: [bool; 6], // W, A, S, D, Space, LShift
    mouse_down: bool,
}

impl FPSCamera {
    fn new(position: Point3<f32>,
           center: Point3<f32>,
           up: Vector3<f32>,
           fov: Deg<f32>,
           aspect: f32)
           -> Self {
        FPSCamera {
            transform: Matrix4::look_at(position, center, up),
            projection: PerspectiveFov {
                fovy: fov.into(),
                aspect: aspect,
                near: 0.1,
                far: 20.0,
            },
            translation: vec3(0.0, 0.0, 0.0),
            rotation: (Rad(0.0), Rad(0.0)),
            mouse_pos: (0, 0),
            mouse_speed: (0.01, 0.01),
            move_speed: 0.01,
            keystates: [false; 6],
            mouse_down: false,
        }
    }

    fn handle_mouse_move(&mut self, x: i32, y: i32) {
        if self.mouse_down {
            let dx = x - self.mouse_pos.0;
            let dy = y - self.mouse_pos.1;
            let yaw = dx as f32 * self.mouse_speed.0;
            let pitch = dy as f32 * self.mouse_speed.1;
            self.rotation.0 += Rad(pitch);
            self.rotation.1 += Rad(yaw);
        }
        self.mouse_pos = (x, y);
    }

    fn handle_mouse_click(&mut self,
                          button: glium::glutin::MouseButton,
                          state: glium::glutin::ElementState) {
        use glium::glutin::ElementState::Pressed;
        use glium::glutin::MouseButton::Left;
        self.mouse_down = button == Left && state == Pressed;
    }

    fn handle_keypress(&mut self,
                       state: glium::glutin::ElementState,
                       keycode: glium::glutin::VirtualKeyCode) {
        use glium::glutin::VirtualKeyCode;
        use glium::glutin::ElementState::Pressed;
        self.keystates[match keycode {
                           VirtualKeyCode::W => 0,
                           VirtualKeyCode::A => 1,
                           VirtualKeyCode::S => 2,
                           VirtualKeyCode::D => 3,
                           VirtualKeyCode::Space => 4,
                           VirtualKeyCode::LShift => 5,
                           _ => return,
                       }] = state == Pressed;
    }

    fn update_view(&mut self, dt: f32) {
        let mut translation = vec3(0.0, 0.0, 0.0);
        if self.keystates[0] {
            translation.y += self.move_speed * dt;
        }
        if self.keystates[1] {
            translation.z += self.move_speed * dt;
        }
        if self.keystates[2] {
            translation.y -= self.move_speed * dt;
        }
        if self.keystates[3] {
            translation.z -= self.move_speed * dt;
        }
        if self.keystates[4] {
            translation.x -= self.move_speed * dt;
        }
        if self.keystates[5] {
            translation.x += self.move_speed * dt;
        }
        self.translation += translation;
    }

    fn get_view_projection(&self) -> (Matrix4<f32>, Matrix4<f32>) {
        (self.transform * Matrix4::from_translation(self.translation) *
         Matrix4::from_angle_z(self.rotation.0) * Matrix4::from_angle_x(self.rotation.1),
         self.projection.into())
    }

    fn pathtracer_view(&self) -> Matrix4<f32> {
        let mut trans = self.translation;
        trans.z *= -1.0;
        trans.y *= -1.0;
        self.transform * Matrix4::from_translation(trans) *
        Matrix4::from_angle_z(self.rotation.0) * Matrix4::from_angle_x(-self.rotation.1)
    }

    fn fov(&self) -> Deg<f32> {
        self.projection.fovy.into()
    }
}
