use super::color::Color;

use std::path::Path;
use std::fmt::Debug;

use num::{Float, NumCast};
use glium::texture::RawImage2d;

#[derive(Clone)]
pub struct Image<T: Float> {
    width: usize,
    height: usize,
    data: Vec<Color<T>>,
}

impl<T> Image<T>
where
    T: Float + Send + Sync + From<f32> + NumCast + Debug,
{
    pub fn new(width: usize, height: usize) -> Self {
        Image {
            width: width,
            height: height,
            data: vec![Color::zero(); width * height],
        }
    }
    pub fn from_buffer(width: usize, height: usize, buffer: Vec<Color<T>>) -> Image<T> {
        assert_eq!(width * height, buffer.len());
        Image {
            width: width,
            height: height,
            data: buffer,
        }
    }
    pub fn data(&self) -> &[Color<T>] {
        &self.data
    }
    fn index(&self, x: usize, y: usize) -> usize {
        if x >= self.width {
            panic!("out of bounds: x = {} !< {}", x, self.width);
        } else if y >= self.height {
            panic!("out of bounds: y = {} !< {}", y, self.height);
        }
        y * self.width + x
    }
    pub fn get(&self, x: usize, y: usize) -> &Color<T> {
        let idx = self.index(x, y);
        &self.data[idx]
    }
    pub fn set(&mut self, x: usize, y: usize, value: Color<T>) {
        let idx = self.index(x, y);
        self.data[idx] = value;
    }
    pub fn add_assign(&mut self, x: usize, y: usize, value: Color<T>) {
        let idx = self.index(x, y);
        self.data[idx] += value;
    }
    pub fn get_dimensions(&self) -> (usize, usize) {
        (self.width, self.height)
    }
    pub fn width(&self) -> usize {
        self.width
    }
    pub fn height(&self) -> usize {
        self.height
    }
    pub fn combine(&mut self, other: &Self, offset: (usize, usize)) {
        let (width, height) = other.get_dimensions();
        assert!(
            self.width >= width + offset.0,
            "self.width: {}, other.width: {}, offset: {}",
            self.width,
            width,
            offset.1
        );
        assert!(
            self.height >= height + offset.1,
            "self.height: {}, other.height: {}, offset: {}",
            self.height,
            height,
            offset.1
        );
        for y in 0..height {
            for x in 0..width {
                self.set(offset.0 + x, offset.1 + y, *other.get(x, y));
            }
        }
    }
    pub fn postprocess(mut self, spp: usize) -> Image<T> {
        for y in 0..self.height {
            for x in 0..self.width {
                let idx = self.index(x, y);
                self.data[idx] = self.data[idx] / (spp as f32).into();
                if ::CONFIG.intermediate.drop_alpha {
                    self.data[idx].a = 1.0.into();
                }
            }
        }
        self
    }
    pub fn store(&self, file: &Path, drop_alpha: bool) {
        use image;

        let mut tmp = Vec::with_capacity(self.width * self.height);
        for pixel in self.data() {
            assert_eq!(pixel, pixel);
            let one = 1.0.into();
            if pixel.r > one || pixel.g > one || pixel.b > one || pixel.a > one {
                warn!("invalid color: {:?}", pixel);
            }
            tmp.push(<u8 as NumCast>::from(pixel.r * 255.0.into()).unwrap());
            tmp.push(<u8 as NumCast>::from(pixel.g * 255.0.into()).unwrap());
            tmp.push(<u8 as NumCast>::from(pixel.b * 255.0.into()).unwrap());
            tmp.push(if drop_alpha {
                255u8
            } else {
                <u8 as NumCast>::from(pixel.a * 255.0.into()).unwrap()
            });
        }
        let image = image::RgbaImage::from_raw(self.width as u32, self.height as u32, tmp).unwrap();
        let image = image::imageops::flip_vertical(&image);
        image.save(file).unwrap();

    }
    // scary
    fn flattened(mut self) -> Vec<T> {
        use std::mem;
        assert_eq!(mem::size_of::<Color<T>>(), 4 * mem::size_of::<T>());
        unsafe {
            let flat = Vec::from_raw_parts(
                self.data.as_mut_ptr() as *mut T,
                self.data.len() * 4,
                self.data.capacity() * 4,
            );
            mem::forget(self.data);
            flat
        }
    }
}

impl<'a> Image<f32> {
    pub fn into_rawimage2d(self) -> RawImage2d<'a, f32> {
        use std::borrow::Cow;
        use glium::texture::ClientFormat;
        let width = self.width;
        RawImage2d {
            width: self.width as u32,
            height: self.height as u32,
            data: Cow::Owned(
                self.flattened()
                    .chunks(width as usize * 4)
                    .rev()
                    .flat_map(|row| row.iter().cloned())
                    .collect(),
            ),
            format: ClientFormat::F32F32F32F32,
        }
    }
}

pub struct AccumulatingImage<T> {
    width: usize,
    height: usize,
    data: Vec<Color<T>>,
}

impl<T> AccumulatingImage<T>
where
    T: Float + Send + Sync + From<f32> + Debug,
{
    pub fn new(width: usize, height: usize) -> Self {
        AccumulatingImage {
            width: width,
            height: height,
            data: vec![Color::zero(); width * height],
        }
    }
    fn index(&self, x: usize, y: usize) -> usize {
        if x >= self.width {
            panic!("out of bounds: x = {} !< {}", x, self.width);
        } else if y >= self.height {
            panic!("out of bounds: y = {} !< {}", y, self.height);
        }
        y * self.width + x
    }
    pub fn get(&self, x: usize, y: usize) -> &Color<T> {
        let idx = self.index(x, y);
        &self.data[idx]
    }
    pub fn add_assign(&mut self, x: usize, y: usize, value: Color<T>) {
        let idx = self.index(x, y);
        self.data[idx] += value;
    }
    pub fn get_dimensions(&self) -> (usize, usize) {
        (self.width, self.height)
    }
    pub fn combine(&mut self, other: &Self, _: (usize, usize)) {
        let (width, height) = other.get_dimensions();
        assert_eq!(
            self.width,
            width,
            "self.width: {}, other.width: {}",
            self.width,
            width
        );
        assert_eq!(
            self.height,
            height,
            "self.height: {}, other.height: {}",
            self.height,
            height
        );
        for y in 0..height {
            for x in 0..width {
                self.add_assign(x, y, *other.get(x, y));
            }
        }
    }
    pub fn postprocess(self, _: usize) -> Image<T> {
        let mut result = Image::new(self.width, self.height);
        for y in 0..self.height {
            for x in 0..self.width {
                let c = self.data[self.index(x, y)];
                let i: f32 = c.a.to_f32().unwrap();
                let mut c = if i <= ::std::f32::EPSILON {
                    warn!(
                        "pre-corrected alpha too close to zero at {}, {}; {:?}",
                        x,
                        y,
                        c
                    );
                    c
                } else {
                    c / c.a
                };
                if ::CONFIG.output.drop_alpha {
                    c.a = 1.0.into();
                }
                result.set(x, y, c);
            }
        }
        result
    }
}
