use super::geometry::Ray;
use super::Mesh;

use rand::Rng;
use cgmath::Point2;
use cgmath::prelude::*;

pub struct Path {
    pub start: Point2<usize>, // on canvas
    pub end: Option<Point2<f32>>, // on the ground
    pub throughput: f32,
    pub n_start: f32,
    pub n_end: f32,
}

impl Path {
    #[allow(float_cmp)]
    pub fn sample_path<R>(
        rng: &mut R,
        ray: Ray,
        start: (usize, usize),
        min_depth: u32,
        max_depth: u32,
        meshes: &[Mesh],
    ) -> Option<Path>
    where
        R: Rng,
    {
        let mut throughput = 1.0;
        let mut ray = ray;
        let n_start = ray.n;

        for depth in 0..max_depth {
            match ray.intersect(meshes) {
                None => {
                    return Some(Path {
                        start: Point2::new(start.0, start.1),
                        end: None,
                        throughput: throughput,
                        n_start: n_start,
                        n_end: ray.n,
                    });
                }
                Some(intersection) => {
                    if let Some(uv) = intersection.ground_uv {
                        return Some(Path {
                            start: Point2::new(start.0, start.1),
                            end: Some(Point2::from_vec(uv)),
                            throughput: throughput,
                            n_start: n_start,
                            n_end: ray.n,
                        });
                    } else {
                        let intersection_point = intersection.position;
                        let n_i = ray.n;
                        let n_o = intersection.n_next;
                        let ratio = n_i / n_o;
                        if n_i == n_o {
                            //not actually a transition
                            ray = Ray::new(intersection_point, ray.dir, n_o);
                        } else {
                            let i = ray.dir; //inciding ray, assumed to be normalized
                            let n = if intersection.from_inside {
                                -intersection.normal
                            } else {
                                intersection.normal
                            };
                            let cos_theta_i = i.dot(n).abs();
                            let r = i + n * cos_theta_i * 2.0; //reflected ray, normalized

                            let sin_theta_t_sq = ratio * ratio * (1.0 - cos_theta_i * cos_theta_i);
                            if sin_theta_t_sq > 1.0 {
                                // total reflection
                                ray = Ray::new(intersection_point, r, n_i);
                            } else {
                                let cos_theta_t = (1.0 - sin_theta_t_sq).sqrt();


                                let r_ortho_sqrt = (ratio * cos_theta_i - cos_theta_t) /
                                    (ratio * cos_theta_i + cos_theta_t);
                                let r_para_sqrt = (cos_theta_i - ratio * cos_theta_t) /
                                    (cos_theta_i + ratio * cos_theta_t);

                                let reflectance =
                                    (r_ortho_sqrt * r_ortho_sqrt + r_para_sqrt * r_para_sqrt) / 2.0;

                                if reflectance >= rng.next_f32() {
                                    ray = Ray::new(intersection_point, r, n_i);
                                    throughput *= reflectance;
                                } else {
                                    let t = (i * ratio + n * (cos_theta_i * ratio - cos_theta_t))
                                        .normalize();
                                    ray = Ray::new(intersection_point, t, n_o);
                                    let transmittance = 1.0 - reflectance;
                                    throughput *= transmittance;
                                }
                            }
                        }
                    }
                }
            }
            if depth > min_depth {
                let survival_chance = f32::min(throughput, 0.9);
                if survival_chance < rng.next_f32() {
                    break;
                } else {
                    throughput /= survival_chance;
                }
            }
        }

        None
    }
}
