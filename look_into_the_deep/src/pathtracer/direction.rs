use super::image::{Image, AccumulatingImage};
use super::path::Path;

use std::fmt::Debug;

use num::{Float, ToPrimitive};

pub trait Direction<T: Send + Sync + Float + Debug> {
    type StoredImage: Send + Sync;
    fn eval_path(&mut self, path: Path, offset: (usize, usize));
    fn skip(&self, x: usize, y: usize) -> bool;
    fn combine(&mut self, other: &Self, offset: (usize, usize));
    fn data(&self) -> &Self::StoredImage;
    fn postprocess(self, spp: usize) -> Image<T>;
}

#[derive(Copy, Clone)]
pub enum Interpolation {
    Nearest,
    Linear,
}

pub struct Forward<'a, T>
where
    T: 'a + Float,
{
    ground: &'a Image<T>,
    to: Image<T>,
    interpolation: Interpolation,
}

impl<'a, T> Forward<'a, T>
where
    T: Copy + Float + From<f32> + Send + Sync + Debug,
{
    pub fn new(
        ground: &'a Image<T>,
        width: usize,
        height: usize,
        interpolation: Interpolation,
    ) -> Self {
        Forward {
            ground: ground,
            to: Image::new(width, height),
            interpolation: interpolation,
        }
    }
}
impl<'a, T> Direction<T> for Forward<'a, T>
where
    T: Copy + Float + From<f32> + Send + Sync + Debug,
{
    type StoredImage = Image<T>;
    fn eval_path(&mut self, path: Path, _: (usize, usize)) {
        match path {
            Path {
                start,
                end: Some(pos),
                throughput,
                n_start,
                n_end,
            } => {
                let throughput = throughput * (n_start * n_start / (n_end * n_end));
                let (w, h) = self.ground.get_dimensions();
                let idx = (pos.x * (w - 1) as f32, pos.y * (h - 1) as f32);
                let color = match self.interpolation {
                    Interpolation::Nearest => {
                        *self.ground.get(
                            idx.0.round() as usize,
                            idx.1.round() as usize,
                        )
                    }
                    Interpolation::Linear => {
                        let ru = *self.ground.get(
                            idx.0.ceil() as usize,
                            idx.1.ceil() as usize,
                        );
                        let rd = *self.ground.get(
                            idx.0.ceil() as usize,
                            idx.1.floor() as usize,
                        );
                        let r_ = rd * (1.0 - idx.1.fract()).into() + ru * idx.1.fract().into();
                        let lu = *self.ground.get(
                            idx.0.floor() as usize,
                            idx.1.ceil() as usize,
                        );
                        let ld = *self.ground.get(
                            idx.0.floor() as usize,
                            idx.1.floor() as usize,
                        );
                        let l_ = ld * (1.0 - idx.1.fract()).into() + lu * idx.1.fract().into();
                        l_ * (1.0 - idx.0.fract()).into() + r_ * idx.0.fract().into()
                    }
                };
                self.to.add_assign(
                    start.x,
                    start.y,
                    color * throughput.into(),
                );
            }
            Path {
                start,
                end: None,
                throughput,
                n_start,
                n_end,
            } => {
                let throughput = throughput * (n_start * n_start / (n_end * n_end));
                self.to.add_assign(
                    start.x,
                    start.y,
                    ::CONFIG.pathtracer.background.to_t() * throughput.into(),
                );
            }
        }
    }
    fn skip(&self, _: usize, _: usize) -> bool {
        false
    }
    fn combine(&mut self, other: &Self, offset: (usize, usize)) {
        self.to.combine(other.data(), offset);
    }
    fn data(&self) -> &Self::StoredImage {
        &self.to
    }
    fn postprocess(self, spp: usize) -> Image<T> {
        self.to.postprocess(spp)
    }
}

pub struct Backward<'a, T>
where
    T: 'a + Float,
{
    ground: AccumulatingImage<T>,
    from: &'a Image<T>,
    interpolation: Interpolation,
}
impl<'a, T> Backward<'a, T>
where
    T: Copy + Float + From<f32> + Send + Sync + Debug,
{
    pub fn new(
        from: &'a Image<T>,
        width: usize,
        height: usize,
        interpolation: Interpolation,
    ) -> Self {
        Backward {
            from: from,
            ground: AccumulatingImage::new(width, height),
            interpolation: interpolation,
        }
    }
}

impl<'a, T> Direction<T> for Backward<'a, T>
where
    T: Copy
        + Float
        + From<f32>
        + Send
        + Sync
        + ToPrimitive
        + Debug,
{
    type StoredImage = AccumulatingImage<T>;
    fn eval_path(&mut self, path: Path, offset: (usize, usize)) {
        match path {
            Path {
                start,
                end: Some(pos),
                throughput,
                ..
            } => {
                let color = *self.from.get(start.x + offset.0, start.y + offset.1) *
                    throughput.into();

                let (w, h) = self.ground.get_dimensions();
                let idx = ((pos.x * (w - 1) as f32), (pos.y * (h - 1) as f32));

                match self.interpolation {
                    Interpolation::Nearest => {
                        self.ground.add_assign(
                            idx.0.round() as usize,
                            idx.1.round() as usize,
                            color,
                        );
                    }
                    Interpolation::Linear => {
                        let x = idx.0.fract();
                        let y = idx.1.fract();
                        self.ground.add_assign(
                            idx.0.ceil() as usize,
                            idx.1.ceil() as usize,
                            color * (x * y).into(),
                        );
                        self.ground.add_assign(
                            idx.0.ceil() as usize,
                            idx.1.floor() as usize,
                            color * (x * (1.0 - y)).into(),
                        );
                        self.ground.add_assign(
                            idx.0.floor() as usize,
                            idx.1.ceil() as usize,
                            color * ((1.0 - x) * y).into(),
                        );
                        self.ground.add_assign(
                            idx.0.floor() as usize,
                            idx.1.floor() as usize,
                            color * ((1.0 - x) * (1.0 - y)).into(),
                        );
                    }
                }
            }
            Path { end: None, .. } => {}
        }
    }
    fn skip(&self, x: usize, y: usize) -> bool {
        let c = *self.from.get(x, y);
        c.a <= ::std::f32::EPSILON.into()
    }
    fn combine(&mut self, other: &Self, offset: (usize, usize)) {
        self.ground.combine(other.data(), offset);
    }
    fn data(&self) -> &Self::StoredImage {
        &self.ground
    }
    fn postprocess(self, spp: usize) -> Image<T> {
        self.ground.postprocess(spp)
    }
}
