#![allow(dead_code)]
use std::ops::{Add, AddAssign, Mul, Div};
use num::Float;


#[derive(Copy, Clone, Debug, PartialEq)]
#[repr(C)]
pub struct Color<T> {
    pub r: T,
    pub g: T,
    pub b: T,
    pub a: T,
}

impl<T: Float> Color<T> {
    pub fn zero() -> Self {
        Color {
            r: T::zero(),
            b: T::zero(),
            g: T::zero(),
            a: T::zero(),
        }
    }
}

impl Color<f32> {
    pub fn to_t<T: From<f32>>(self) -> Color<T> {
        Color {
            r: self.r.into(),
            g: self.g.into(),
            b: self.b.into(),
            a: self.a.into(),
        }
    }
}

impl<T: Add<Output = T>> Add for Color<T> {
    type Output = Color<T>;
    fn add(self, rhs: Color<T>) -> Color<T> {
        let Color {
            r: r1,
            g: g1,
            b: b1,
            a: a1,
        } = self;
        let Color {
            r: r2,
            g: g2,
            b: b2,
            a: a2,
        } = rhs;
        Color {
            r: r1 + r2,
            g: g1 + g2,
            b: b1 + b2,
            a: a1 + a2,
        }
    }
}

impl<T: Copy + Add<Output = T>> AddAssign for Color<T> {
    fn add_assign(&mut self, other: Self) {
        self.r = self.r + other.r;
        self.g = self.g + other.g;
        self.b = self.b + other.b;
        self.a = self.a + other.a;
    }
}

impl<T: Copy + Mul<Output = T>> Mul<T> for Color<T> {
    type Output = Color<T>;
    fn mul(self, rhs: T) -> Self::Output {
        let Color { r, g, b, a } = self;
        Color {
            r: r * rhs,
            g: g * rhs,
            b: b * rhs,
            a: a * rhs,
        }
    }
}

impl<T: Copy + Div<Output = T>> Div<T> for Color<T> {
    type Output = Color<T>;
    fn div(self, rhs: T) -> Self::Output {
        let Color { r, g, b, a } = self;
        Color {
            r: r / rhs,
            g: g / rhs,
            b: b / rhs,
            a: a / rhs,
        }
    }
}

impl<T> From<(T, T, T, T)> for Color<T> {
    fn from(c: (T, T, T, T)) -> Self {
        Color {
            r: c.0,
            g: c.1,
            b: c.2,
            a: c.3,
        }
    }
}

impl<T> Into<(T, T, T, T)> for Color<T> {
    fn into(self) -> (T, T, T, T) {
        (self.r, self.g, self.b, self.a)
    }
}
