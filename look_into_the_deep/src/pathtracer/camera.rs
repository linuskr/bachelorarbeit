use rand::ThreadRng;
use super::geometry::ray::Ray;
use std::marker::PhantomData;
use cgmath::prelude::*;
use cgmath::{Point3, Vector3, Deg, vec3};

pub struct Camera {
    position: Point3<f32>,
    look_at: Vector3<f32>,
    up: Vector3<f32>,
    right: Vector3<f32>,
    canvas_size_half: (f32, f32),
    pixel_size: (f32, f32),
    pub canvas_dim_px: (usize, usize),
    pub spp: usize,
}


impl Camera {
    pub fn make_ray(&self,
                    pixel_pos: (usize, usize),
                    mut rng: &mut ThreadRng,
                    n_start: f32)
                    -> Ray {
        use rand::distributions::{IndependentSample, Range};
        let range = Range::new(-0.5f32, 0.5);

        let (pos_x, pos_y) = pixel_pos;
        let (mut pos_x, mut pos_y) = (pos_x as f32, pos_y as f32);
        if self.spp > 1 {
            pos_x += range.ind_sample(&mut rng);
            pos_y += range.ind_sample(&mut rng);
        }
        pos_x = self.pixel_size.0 * pos_x - self.canvas_size_half.0;
        pos_y = self.pixel_size.1 * pos_y - self.canvas_size_half.1;
        let dir = self.look_at - self.right * pos_x - self.up * pos_y;
        Ray::new(self.position, dir.normalize(), n_start)
    }
}

pub struct CameraBuilder<A, B> {
    position: Point3<f32>,
    center: Point3<f32>,
    up: Vector3<f32>,
    fov: Deg<f32>,
    canvas_dim_px: (usize, usize),
    spp: usize,
    has_pos: PhantomData<A>,
    has_dim: PhantomData<B>,
}
impl CameraBuilder<(), ()> {
    pub fn new() -> Self {
        CameraBuilder {
            position: Point3::new(0.0, 0.0, 0.0),
            center: Point3::new(0.0, 0.0, 0.0),
            up: vec3(0.0, 0.0, 0.0),
            fov: Deg(75.0),
            canvas_dim_px: (0, 0),
            spp: 1,
            has_pos: PhantomData,
            has_dim: PhantomData,
        }
    }
}
impl<A, B> CameraBuilder<A, B> {
    pub fn position_center_and_up(self,
                                  position: Point3<f32>,
                                  center: Point3<f32>,
                                  up: Vector3<f32>)
                                  -> CameraBuilder<(()), B> {
        CameraBuilder {
            position: position,
            center: center,
            up: up,
            fov: self.fov,
            canvas_dim_px: self.canvas_dim_px,
            spp: self.spp,
            has_pos: PhantomData,
            has_dim: self.has_dim,
        }
    }
    pub fn with_fov_deg(mut self, fov: f32) -> Self {
        self.fov = Deg(fov);
        self
    }
    #[allow(dead_code)]
    pub fn with_fov_rad(mut self, fov: f32) -> Self {
        use cgmath::Rad;
        self.fov = Deg::from(Rad(fov));
        self
    }
    pub fn canvas_resolution_in_px(self, width: usize, height: usize) -> CameraBuilder<A, (())> {
        CameraBuilder {
            position: self.position,
            center: self.center,
            up: self.up,
            fov: self.fov,
            canvas_dim_px: (width, height),
            spp: self.spp,
            has_pos: self.has_pos,
            has_dim: PhantomData,
        }
    }
    pub fn with_spp(mut self, samples: usize) -> Self {
        assert!(samples > 0);
        self.spp = samples;
        self
    }
}
impl CameraBuilder<(()), (())> {
    pub fn create(self) -> Camera {
        let look_at = (self.center - self.position).normalize();
        let right = self.up.cross(look_at).normalize();
        let up = right.cross(look_at).normalize();
        let canvas_size_half = {
            let aspect_ratio = self.canvas_dim_px.1 as f32 / self.canvas_dim_px.0 as f32;
            let size_x = look_at.magnitude() * (self.fov / 2.0).tan();
            let size_y = size_x * aspect_ratio;
            (size_x, size_y)
        };
        let canvas_size = (canvas_size_half.0 * 2.0, canvas_size_half.1 * 2.0);
        let pixel_size = (canvas_size.0 / self.canvas_dim_px.0 as f32,
                          canvas_size.1 / self.canvas_dim_px.1 as f32);
        Camera {
            position: self.position,
            look_at: look_at,
            up: up,
            right: right,
            canvas_size_half: canvas_size_half,
            pixel_size: pixel_size,
            canvas_dim_px: self.canvas_dim_px,
            spp: self.spp,
        }
    }
}
