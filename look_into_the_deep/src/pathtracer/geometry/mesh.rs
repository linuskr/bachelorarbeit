extern crate tobj;

use super::Cell;
use pathtracer::geometry::ray::{Ray, RayIntersection};
use renderer::Vertex3D;
use tray_rust::geometry::BVH;
use tray_rust::geometry::triangle::Triangle;
use std::sync::Arc;
use std::path::Path;
use cgmath::{Point3, Vector3, Vector2, vec3, vec2};

pub struct Mesh {
    bvh: BVH<Cell>,
    pub vertex_data: Vec<Vertex3D>,
    pub is_ground: bool,
}

impl Mesh {
    #[allow(too_many_arguments)]
    pub fn new(positions: Arc<Vec<Point3<f32>>>,
               normals: Arc<Vec<Vector3<f32>>>,
               texcoords: Arc<Vec<Vector2<f32>>>,
               indices: &[u32],
               n_inside: f32,
               n_outside: f32,
               is_ground: bool,
               vertex_data: Vec<Vertex3D>)
               -> Self {
        let triangle_cells = indices
            .chunks(3)
            .map(|i| {
                     Triangle::new(i[0] as usize,
                                   i[1] as usize,
                                   i[2] as usize,
                                   positions.clone(),
                                   normals.clone(),
                                   texcoords.clone())
                 })
            .map(|t| Cell::new(n_inside, n_outside, t, is_ground))
            .collect::<Vec<_>>();
        Mesh {
            bvh: BVH::unanimated(64, triangle_cells),
            vertex_data: vertex_data,
            is_ground: is_ground,
        }
    }

    fn from_model(m: &tobj::Model,
                  is_ground: bool,
                  n_inside: f32,
                  n_outside: f32)
                  -> Result<Self, String> {
        let mesh = &m.mesh;
        if mesh.normals.is_empty() || mesh.texcoords.is_empty() {
            return Err(format!("Mesh::load_obj error! normals and texture coordinates are \
                                required!\nSkipping {}",
                               m.name));
        }
        debug!("{} has {} triangles", m.name, mesh.indices.len() / 3);
        let positions = Arc::new(mesh.positions
                                     .chunks(3)
                                     .map(|i| Point3::new(i[0], i[1], i[2]))
                                     .collect());
        let normals = Arc::new(mesh.normals
                                   .chunks(3)
                                   .map(|i| vec3(i[0], i[1], i[2]))
                                   .collect());
        let texcoords = Arc::new(mesh.texcoords.chunks(2).map(|i| vec2(i[0], i[1])).collect());
        let mut vertex_data = Vec::new();
        for idx in &mesh.indices {
            let i = *idx as usize;
            let pos = [mesh.positions[3 * i],
                       mesh.positions[3 * i + 1],
                       mesh.positions[3 * i + 2]];
            let normal = if is_ground {
                [0.0; 3]
            } else {
                [mesh.normals[3 * i],
                 mesh.normals[3 * i + 1],
                 mesh.normals[3 * i + 2]]
            };
            let tex_coords = [mesh.texcoords[2 * i], mesh.texcoords[2 * i + 1]];

            vertex_data.push(Vertex3D {
                                 position: pos,
                                 normal: normal,
                                 tex_coord: tex_coords,
                             });
        }
        Ok(Mesh::new(positions,
                     normals,
                     texcoords,
                     &mesh.indices,
                     n_inside,
                     n_outside,
                     is_ground,
                     vertex_data))
    }

    pub fn load_obj(file_name: &Path,
                    n_inside: f32,
                    n_outside: f32,
                    meshnames_fwd: Vec<String>,
                    meshnames_bwd: Vec<String>)
                    -> (Vec<Mesh>, usize, usize) {
        let (mut bwd, mut fwd) = (0, 0);
        match tobj::load_obj(file_name) {
            Ok((models, _)) => {
                let mut meshes = Vec::new();
                for m in models
                        .iter()
                        .filter(|m| meshnames_fwd.iter().any(|s| m.name.contains(s))) {
                    info!("Loading model {} for forward", m.name);
                    match Mesh::from_model(m, false, n_inside, n_outside) {
                        Ok(mesh) => meshes.push(mesh),
                        Err(e) => warn!("{}", e),
                    }
                }
                bwd = meshes.len();
                for m in models.iter().filter(|m| m.name.contains("ground")) {
                    info!("Loading model {} as ground", m.name);
                    match Mesh::from_model(m, true, n_inside, n_outside) {
                        Ok(mesh) => meshes.push(mesh),
                        Err(e) => warn!("{}", e),
                    }
                }
                fwd = meshes.len();
                for m in models
                        .iter()
                        .filter(|m| meshnames_bwd.iter().any(|s| m.name.contains(s))) {
                    info!("Loading model {} for backward", m.name);
                    match Mesh::from_model(m, false, n_inside, n_outside) {
                        Ok(mesh) => meshes.push(mesh),
                        Err(e) => warn!("{}", e),
                    }
                }
                (meshes, bwd, fwd)
            }
            Err(e) => {
                warn!("Failed to load {:?} due to {:?}", file_name, e);
                (Vec::new(), bwd, fwd)
            }
        }
    }

    pub fn intersect(&self, ray: &mut Ray) -> Option<RayIntersection> {
        self.bvh.intersect(ray, |r, i| i.intersect(r))
    }
}
