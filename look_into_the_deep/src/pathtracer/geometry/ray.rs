use cgmath::{Point3, Vector3, Vector2};
use std::f32;
use super::Mesh;

#[derive(Clone, Debug)]
pub struct Ray {
    pub origin: Point3<f32>,
    pub dir: Vector3<f32>,
    /// Point along the ray that the actual ray starts at, `p = o + min_t * d`
    pub min_t: f32,
    /// Point along the ray at which it stops, will be inf if the ray is infinite
    pub max_t: f32,
    pub n: f32,
}

impl Ray {
    pub fn new(origin: Point3<f32>, direction: Vector3<f32>, n_start: f32) -> Self {
        Ray {
            origin: origin,
            dir: direction,
            min_t: 0f32,
            max_t: f32::INFINITY,
            n: n_start,
        }
    }
    pub fn at(&self, t: f32) -> Point3<f32> {
        self.origin + self.dir * t
    }
    pub fn intersect(&mut self, meshes: &[Mesh]) -> Option<RayIntersection> {
        let mut intersection: Option<RayIntersection> = None;
        for mesh in meshes {
            if let Some(hit) = mesh.intersect(self) {
                match intersection {
                    Some(tmp) => {
                        if hit.t < tmp.t {
                            intersection = Some(hit)
                        } else {
                            intersection = Some(tmp)
                        }
                    }
                    None => intersection = Some(hit),
                }
            }
        }
        intersection
    }
}

pub struct RayIntersection {
    pub t: f32,
    pub position: Point3<f32>,
    pub normal: Vector3<f32>,
    pub n_current: f32,
    pub n_next: f32,
    pub ground_uv: Option<Vector2<f32>>,
    pub from_inside: bool,
}
