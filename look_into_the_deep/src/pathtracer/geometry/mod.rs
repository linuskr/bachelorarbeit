use tray_rust::geometry::{Geometry, Boundable, BBox};
use tray_rust::geometry::triangle::Triangle;
pub use self::ray::{Ray, RayIntersection};
pub use self::mesh::Mesh;

pub mod ray;
pub mod mesh;

pub struct Cell {
    pub n_inside: f32,
    pub n_outside: f32,
    triangle: Triangle,
    pub is_ground: bool,
}

impl Cell {
    pub fn new(n_inside: f32, n_outside: f32, triangle: Triangle, is_ground: bool) -> Self {
        Cell {
            n_inside: n_inside,
            n_outside: n_outside,
            triangle: triangle,
            is_ground: is_ground,
        }
    }

    pub fn intersect(&self, ray: &mut Ray) -> Option<RayIntersection> {
        self.triangle.intersect(ray).map(|dg| {
            RayIntersection {
                t: dg.t,
                position: dg.p,
                normal: dg.n,
                n_current: if dg.from_inside {
                    self.n_inside
                } else {
                    self.n_outside
                },
                n_next: if dg.from_inside {
                    self.n_outside
                } else {
                    self.n_inside
                },
                ground_uv: if self.is_ground { Some(dg.uv) } else { None },
                from_inside: dg.from_inside,
            }
        })
    }
}

impl Boundable for Cell {
    fn bounds(&self, start: f32, end: f32) -> BBox {
        self.triangle.bounds(start, end)
    }
}
