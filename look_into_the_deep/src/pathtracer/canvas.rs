use super::camera::Camera;
use super::geometry::Mesh;
use super::direction::Direction;

use std::marker::{Send, Sync};
use std::fmt::Debug;
use num::Float;
use super::path::Path;


pub struct Canvas<'a> {
    pub dim: (usize, usize),
    offset: (usize, usize),
    camera: &'a Camera,
    meshes: &'a [Mesh],
    max_iterations: u32,
}



impl<'a> Canvas<'a> {
    pub fn new(dim: (usize, usize),
               offset: (usize, usize),
               camera: &'a Camera,
               meshes: &'a [Mesh],
               max_iterations: u32)
               -> Self {
        Canvas {
            dim: dim,
            offset: offset,
            camera: camera,
            meshes: meshes,
            max_iterations: max_iterations,
        }
    }


    pub fn render<T, U>(&self, mut direction: U, n_start: f32) -> ((usize, usize), U)
        where T: Copy + Float + From<f32> + Send + Sync + Debug,
              U: Direction<T>
    {
        use rand;
        let mut rng = rand::thread_rng();
        let (off_x, off_y) = self.offset;

        for x in 0..self.dim.0 {
            for y in 0..self.dim.1 {
                if !direction.skip(off_x + x, off_y + y) {
                    for _ in 1..self.camera.spp {
                        let ray = self.camera
                            .make_ray((off_x + x, off_y + y), &mut rng, n_start);
                        if let Some(path) = Path::sample_path(&mut rng,
                                                              ray,
                                                              (x, y),
                                                              5,
                                                              self.max_iterations,
                                                              self.meshes) {
                            direction.eval_path(path, self.offset);
                        }
                    }
                }
            }
        }
        (self.offset, direction)
    }
}





pub struct CanvasIter<'a> {
    delta: (usize, usize),
    current_offset: (usize, usize),
    width: usize,
    height: usize,
    camera: &'a Camera,
    meshes: &'a [Mesh],
    max_iterations: u32,
}

impl<'a> CanvasIter<'a> {
    pub fn new(tilewidth: usize,
               tileheight: usize,
               camera: &'a Camera,
               meshes: &'a [Mesh],
               max_iterations: u32)
               -> Self {

        let (width, height) = camera.canvas_dim_px;
        CanvasIter {
            delta: (tilewidth, tileheight),
            current_offset: (0, 0),
            width: width,
            height: height,
            camera: camera,
            meshes: meshes,
            max_iterations: max_iterations,
        }
    }
}

impl<'a> Iterator for CanvasIter<'a> {
    type Item = Canvas<'a>;

    fn next(&mut self) -> Option<Canvas<'a>> {
        let (x, y) = self.current_offset;
        let (mut dx, mut dy) = self.delta;

        if x >= self.width {
            return None;
        }

        if x + dx >= self.width && x < self.width {
            dx = self.width - x;
        }

        if y + dy < self.height {
            self.current_offset.1 += dy;
        } else if y < self.height {
            dy = self.height - y;
            self.current_offset.0 += dx;
            self.current_offset.1 = 0;
        }
        Some(Canvas::new((dx, dy),
                         (x, y),
                         self.camera,
                         self.meshes,
                         self.max_iterations))

    }
}


#[cfg(test)]
mod tests {

    #[test]
    fn image_index_test() {
        use super::Image;
        use super::super::Color;

        let width = 1000;
        let height = 300;
        let mut buf = Vec::with_capacity(width * height);
        for y in 0..height {
            for x in 0..width {
                buf.push(Color {
                             r: x,
                             g: y,
                             b: 0,
                             a: 1,
                         });
            }
        }
        let image = Image::from_buffer(width, height, buf);
        for y in 0..height {
            for x in 0..width {
                assert_eq!(image[(x, y)],
                           Color {
                               r: x,
                               g: y,
                               b: 0,
                               a: 1,
                           });
            }
        }
    }

    #[test]
    fn canvas_iter_single_fit() {
        use super::*;
        use super::super::Color;
        use super::super::CameraBuilder;
        use cgmath::{Point3, vec3};

        let width = 100;
        let height = 20;
        let tilewidth = 100;
        let tileheight = 20;
        let camera = CameraBuilder::new()
            .position_center_and_up(Point3::new(0.0, 10.0, 0.0),
                                    Point3::new(0.0, 0.0, 0.0),
                                    vec3(1.0, 0.0, 0.0))
            .with_fov_deg(75.0)
            .canvas_resolution_in_px(width, height)
            .with_spp(0)
            .in_optical_density(1.0)
            .create();
        let meshes = Vec::new();

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 1);
        assert_eq!(canvases[0].size_and_offset(), ((width, height), (0, 0)));

    }

    #[test]
    fn canvas_iter_single_oversized() {
        use super::*;
        use super::super::Color;
        use super::super::CameraBuilder;
        use cgmath::{Point3, vec3};

        let width = 100;
        let height = 20;
        let tilewidth = 120;
        let tileheight = 20;
        let camera = CameraBuilder::new()
            .position_center_and_up(Point3::new(0.0, 10.0, 0.0),
                                    Point3::new(0.0, 0.0, 0.0),
                                    vec3(1.0, 0.0, 0.0))
            .with_fov_deg(75.0)
            .canvas_resolution_in_px(width, height)
            .with_spp(0)
            .in_optical_density(1.0)
            .create();
        let meshes = Vec::new();

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 1);
        assert_eq!(canvases[0].size_and_offset(), ((width, height), (0, 0)));

        let width = 100;
        let height = 20;
        let tilewidth = 100;
        let tileheight = 40;

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 1);
        assert_eq!(canvases[0].size_and_offset(), ((width, height), (0, 0)));

        let width = 100;
        let height = 20;
        let tilewidth = 120;
        let tileheight = 50;

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 1);
        assert_eq!(canvases[0].size_and_offset(), ((width, height), (0, 0)));
    }

    #[test]
    fn canvas_iter_multi_fit() {
        use super::*;
        use super::super::Color;
        use super::super::CameraBuilder;
        use cgmath::{Point3, vec3};

        let width = 100;
        let height = 20;
        let tilewidth = 10;
        let tileheight = 20;
        let camera = CameraBuilder::new()
            .position_center_and_up(Point3::new(0.0, 10.0, 0.0),
                                    Point3::new(0.0, 0.0, 0.0),
                                    vec3(1.0, 0.0, 0.0))
            .with_fov_deg(75.0)
            .canvas_resolution_in_px(width, height)
            .with_spp(0)
            .in_optical_density(1.0)
            .create();
        let meshes = Vec::new();

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 10);
        assert_eq!(canvases[0].size_and_offset(),
                   ((tilewidth, tileheight), (0, 0)));
        assert_eq!(canvases[9].size_and_offset(),
                   ((tilewidth, tileheight), (90, 0)));

        let width = 100;
        let height = 20;
        let tilewidth = 100;
        let tileheight = 10;

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 2);
        assert_eq!(canvases[0].size_and_offset(),
                   ((tilewidth, tileheight), (0, 0)));
        assert_eq!(canvases[1].size_and_offset(),
                   ((tilewidth, tileheight), (0, 10)));

        let width = 100;
        let height = 20;
        let tilewidth = 10;
        let tileheight = 10;

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 20);
        assert_eq!(canvases[0].size_and_offset(),
                   ((tilewidth, tileheight), (0, 0)));
        assert_eq!(canvases[19].size_and_offset(),
                   ((tilewidth, tileheight), (90, 10)));
    }

    #[test]
    fn canvas_iter_multi_nofit() {
        use super::*;
        use super::super::Color;
        use super::super::CameraBuilder;
        use cgmath::{Point3, vec3};

        let width = 105;
        let height = 20;
        let tilewidth = 10;
        let tileheight = 20;
        let camera = CameraBuilder::new()
            .position_center_and_up(Point3::new(0.0, 10.0, 0.0),
                                    Point3::new(0.0, 0.0, 0.0),
                                    vec3(1.0, 0.0, 0.0))
            .with_fov_deg(75.0)
            .canvas_resolution_in_px(width, height)
            .with_spp(0)
            .in_optical_density(1.0)
            .create();
        let meshes = Vec::new();

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 11);
        assert_eq!(canvases[0].size_and_offset(),
                   ((tilewidth, tileheight), (0, 0)));
        assert_eq!(canvases[9].size_and_offset(),
                   ((tilewidth, tileheight), (90, 0)));
        assert_eq!(canvases[10].size_and_offset(),
                   ((width % tilewidth, tileheight), (100, 0)));

        let width = 100;
        let height = 25;
        let tilewidth = 100;
        let tileheight = 10;

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 3);
        assert_eq!(canvases[0].size_and_offset(),
                   ((tilewidth, tileheight), (0, 0)));
        assert_eq!(canvases[1].size_and_offset(),
                   ((tilewidth, tileheight), (0, 10)));
        assert_eq!(canvases[2].size_and_offset(),
                   ((tilewidth, height % tileheight), (0, 20)));

        let width = 105;
        let height = 25;
        let tilewidth = 10;
        let tileheight = 10;

        let canvas_iter = CanvasIter::<f32>::new(width,
                                                 height,
                                                 tilewidth,
                                                 tileheight,
                                                 Color::zero(),
                                                 &camera,
                                                 &meshes);
        let canvases = canvas_iter.collect::<Vec<_>>();
        assert_eq!(canvases.len(), 33);
        assert_eq!(canvases[0].size_and_offset(),
                   ((tilewidth, tileheight), (0, 0)));
        assert_eq!(canvases[2].size_and_offset(),
                   ((tilewidth, height % tileheight), (0, 20)));
        assert_eq!(canvases[30].size_and_offset(),
                   ((width % tilewidth, tileheight), (100, 0)));
        assert_eq!(canvases[32].size_and_offset(),
                   ((width % tilewidth, height % tileheight), (100, 20)));
    }
}
