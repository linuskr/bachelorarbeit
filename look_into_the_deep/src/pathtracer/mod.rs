extern crate num_cpus;
extern crate scoped_threadpool;

pub mod color;
pub mod camera;
pub mod geometry;
pub mod direction;
pub mod image;
mod canvas;
mod path;

pub use self::color::Color;
pub use self::camera::{Camera, CameraBuilder};
pub use self::image::Image;
pub use self::direction::{Direction, Forward, Backward};

use self::canvas::CanvasIter;
use self::geometry::Mesh;
use config::{PathtracerConfig, MeshConfig};
use cgmath::{Point3, Vector3};

use std::sync::mpsc;
use std::fmt;

use num::Float;


#[derive(Copy, Clone)]
pub enum RenderDirection {
    Forward,
    Backward,
}

impl fmt::Display for RenderDirection {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}", match *self {
            RenderDirection::Forward => "fwd",
            RenderDirection::Backward => "bwd",
        })
    }
}


pub fn run<'a, T, U, F>(camera: &Camera,
                        meshes: &'a [Mesh],
                        config: &'a PathtracerConfig,
                        direction: RenderDirection,
                        direction_builder: F,
                        ground_res: (usize, usize),
                        n_start: f32)
                        -> Image<T>
    where T: Copy + Float + From<f32> + Send + Sync + fmt::Debug,
          U: Direction<T> + Send,
          F: Fn((usize, usize)) -> U + Send + Sync
{
    use self::scoped_threadpool::Pool;
    use self::RenderDirection::{Forward, Backward};
    let tiledim = (config.tilewidth, config.tileheight);
    let tiles = CanvasIter::new(tiledim.0, tiledim.1, camera, meshes, config.max_iterations)
        .collect::<Vec<_>>();

    let mut thread_count = config.threads;
    if thread_count == 0 {
        thread_count = num_cpus::get();
    }
    let channel_size = {
        let buffer_size = config.buffersize * 1024 * 1024;
        let tile_size = 4 * 4 *
                        match direction {
                            Forward => tiledim.0 * tiledim.1,
                            Backward => ground_res.0 * ground_res.1,
                        };
        buffer_size / tile_size
    };
    let (result_tx, result_rx) = mpsc::sync_channel(channel_size);

    debug!("{} threads, {} tiles, {} elements buffer",
           thread_count,
           tiles.len(),
           channel_size);

    let mut result_image = direction_builder(match direction {
                                                 Forward => camera.canvas_dim_px,
                                                 Backward => ground_res,
                                             });
    let direction_builder = &direction_builder;
    let mut pool = Pool::new(thread_count as u32);
    pool.scoped(|scope| {
        let tilecount = tiles.len();
        for tile in tiles {
            let result_tx = result_tx.clone();
            scope.execute(move || {
                let to_dim = match direction {
                    Forward => tile.dim,
                    Backward => ground_res,
                };
                result_tx
                    .send(tile.render(direction_builder(to_dim), n_start))
                    .unwrap();
            });
        }
        for i in 0..tilecount {
            let result = result_rx.recv().unwrap();
            let (offset, image) = result;
            result_image.combine(&image, offset);
            info!("Tracing: {:?}% completed, last: {}, {}",
                  (i + 1) as f32 / tilecount as f32 * 100.0,
                  offset.0,
                  offset.1);
        }
    });
    info!("Tracing completed");
    result_image.postprocess(camera.spp)
}


pub fn raytrace<T>(config: &PathtracerConfig,
                   cameraparam: &CameraParams,
                   image: &Image<T>,
                   meshcfg: &MeshConfig,
                   direction: RenderDirection,
                   result_dim: (usize, usize),
                   n_start: f32)
                   -> Image<T>
    where T: Float + Send + Sync + From<f32> + fmt::Debug
{

    info!("running in {} mode", match direction {
        RenderDirection::Forward => "forward",
        RenderDirection::Backward => "backward",
    });

    let (output_width, output_height) = result_dim;

    let canvas_width = if let RenderDirection::Forward = direction {
        output_width
    } else {
        image.get_dimensions().0
    };
    let canvas_height = if let RenderDirection::Forward = direction {
        output_height
    } else {
        image.get_dimensions().1
    };

    let camera = CameraBuilder::new()
        .position_center_and_up(cameraparam.pos, cameraparam.center, cameraparam.up)
        .with_fov_deg(cameraparam.fov)
        .canvas_resolution_in_px(canvas_width, canvas_height)
        .with_spp(config.spp)
        .create();
    match direction {
        RenderDirection::Forward => {
            use pathtracer::Forward;
            run(&camera,
                &meshcfg.meshes[..meshcfg.idx_fwd],
                config,
                direction,
                |dim| Forward::new(image, dim.0, dim.1, config.interpolation),
                (output_width, output_height),
                n_start)
        }
        RenderDirection::Backward => {
            use pathtracer::Backward;
            run(&camera,
                &meshcfg.meshes[meshcfg.idx_bwd..],
                config,
                direction,
                |dim| Backward::new(image, dim.0, dim.1, config.interpolation),
                (output_width, output_height),
                n_start)
        }
    }
}

pub struct CameraParams {
    pub pos: Point3<f32>,
    pub center: Point3<f32>,
    pub up: Vector3<f32>,
    pub fov: f32,
}
