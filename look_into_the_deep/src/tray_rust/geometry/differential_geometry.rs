//! Defines the `DifferentialGeometry` type which is used to pass information
//! about the hit piece of geometry back from the intersection to the shading

use tray_rust::geometry::Geometry;
use cgmath::{Point3, Vector3, Vector2};

/// Stores information about a hit piece of geometry of some object in the scene
#[derive(Clone, Copy)]
pub struct DifferentialGeometry<'a> {
    /// How far we've travelled along the ray
    pub t: f32,
    /// The hit point
    pub p: Point3<f32>,
    /// The shading normal
    pub n: Vector3<f32>,
    /// The geometry normal
    pub ng: Vector3<f32>,
    /// uv coordinates of the hit
    pub uv: Vector2<f32>,
    /// was the geometry hit from the inside
    pub from_inside: bool,
    /// The geometry that was hit
    pub geom: &'a (Geometry + 'a),
}

impl<'a> DifferentialGeometry<'a> {
    /// Setup the differential geometry using the normal passed for the surface normal
    pub fn with_normal(t: f32,
                       p: Point3<f32>,
                       n: Vector3<f32>,
                       uv: Vector2<f32>,
                       from_inside: bool,
                       geom: &'a (Geometry + 'a))
                       -> DifferentialGeometry<'a> {
        DifferentialGeometry {
            t: t,
            p: p,
            n: n,
            ng: n,
            uv: uv,
            from_inside: from_inside,
            geom: geom,
        }
    }
}
