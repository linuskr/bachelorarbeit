//! Defines a triangle mesh geometry. Intersection tests are accelerated internally
//! by storing the triangles of the mesh in a BVH
//!
//! # Scene Usage Example
//! The mesh is specified by the OBJ file to load and the name of the specific
//! model within the file to use. The file and other loaded models are kept loaded
//! so you can easily use the same or other models in the file as well. If no name is
//! assigned to the model in the file it will be given the name `"unnamed_model"`,
//! however it's recommended to name your models.
//!
//! ```json
//! "geometry": {
//!     "type": "mesh",
//!     "file": "./suzanne.obj",
//!     "model": "Suzanne"
//! }
//! ```

extern crate tobj;

use std::sync::Arc;

use tray_rust::geometry::{Geometry, DifferentialGeometry, Boundable, BBox};
use pathtracer::geometry::Ray;
use cgmath::prelude::*;
use cgmath::{Point3, Vector3, Vector2};

/// A triangle in some mesh. Just stores a reference to the mesh
/// and the indices of each vertex
pub struct Triangle {
    a: usize,
    b: usize,
    c: usize,
    positions: Arc<Vec<Point3<f32>>>,
    normals: Arc<Vec<Vector3<f32>>>,
    texcoords: Arc<Vec<Vector2<f32>>>,
}

impl Triangle {
    /// Create a new triangle representing a triangle within the mesh passed
    pub fn new(a: usize,
               b: usize,
               c: usize,
               positions: Arc<Vec<Point3<f32>>>,
               normals: Arc<Vec<Vector3<f32>>>,
               texcoords: Arc<Vec<Vector2<f32>>>)
               -> Triangle {
        Triangle {
            a: a,
            b: b,
            c: c,
            positions: positions,
            normals: normals,
            texcoords: texcoords,
        }
    }
}

// Möller-Trumbore triangle
impl Geometry for Triangle {
    fn intersect(&self, ray: &mut Ray) -> Option<DifferentialGeometry> {
        use std::f32;

        let pa = self.positions[self.a];
        let pb = self.positions[self.b];
        let pc = self.positions[self.c];

        let e = [pb - pa, pc - pa];
        let mut s = [Vector3::from_value(0.0); 2]; // [P, Q]
        s[0] = ray.dir.cross(e[1]); // P
        let inv_det = {
            let d = s[0].dot(e[0]); // determinant
            if d < f32::EPSILON && d > -f32::EPSILON {
                // degenerate triangle, can't hit
                return None;
            }
            1.0 / d
        };

        let x: Point3<f32> = ray.origin;
        let d = x - pa;
        let mut bary = [0.0; 3];
        bary[1] = d.dot(s[0]) * inv_det;
        // Check that the first barycentric coordinate is in the triangle bounds
        if bary[1] < 0.0 || bary[1] > 1.0 {
            return None;
        }

        s[1] = d.cross(e[0]);
        bary[2] = ray.dir.dot(s[1]) * inv_det;
        // Check the second barycentric coordinate is in the triangle bounds
        if bary[2] < 0.0 || bary[1] + bary[2] > 1.0 {
            return None;
        }

        // We've hit the triangle with the ray, now check the hit location is in the ray range
        let t = e[1].dot(s[1]) * inv_det;
        if t <= ray.min_t + f32::EPSILON || t > ray.max_t {
            return None;
        }
        bary[0] = 1.0 - bary[1] - bary[2];
        ray.max_t = t;
        let p = ray.at(t);

        // Now compute normal at this location on the triangle
        let na = self.normals[self.a];
        let nb = self.normals[self.b];
        let nc = self.normals[self.c];
        let n = (bary[0] * na + bary[1] * nb + bary[2] * nc).normalize();

        // Compute parameterization of surface and various derivatives for texturing
        // Triangles are parameterized by the obj texcoords at the vertices
        let ta = self.texcoords[self.a];
        let tb = self.texcoords[self.b];
        let tc = self.texcoords[self.c];
        let uv = bary[0] * ta + bary[1] * tb + bary[2] * tc;
        let from_inside = {
            let mut tri_normal = e[0].cross(e[1]);
            if tri_normal.dot(n) < 0.0 {
                tri_normal = -tri_normal;
            }
            tri_normal.dot(ray.dir) > 0.0
        };
        Some(DifferentialGeometry::with_normal(t, p, n, uv, from_inside, self))
    }
}

impl Boundable for Triangle {
    fn bounds(&self, _: f32, _: f32) -> BBox {
        BBox::singular(self.positions[self.a])
            .point_union(&self.positions[self.b])
            .point_union(&self.positions[self.c])
    }
}
