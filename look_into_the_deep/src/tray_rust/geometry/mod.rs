//! The geometry module defines the Geometry trait implemented by
//! the various geometry in the ray tracer and provides some standard
//! geometry for rendering
//!
//! # Scene Usage Example
//! All geometry will appear within an object specification and requires the type
//! of geometry being specified along with any parameters for that geometry.
//!
//! An instance has a geometry along with additional information like a material
//! and transformation to place it in the world, see the instance module for more.
//!
//! ```json
//! "objects": [
//!     {
//!          "type": "The_Instance_Type",
//!          ...
//!          "geometry": {
//!              "type": "The_Geometry_Type",
//!              ...
//!          }
//!     },
//!     ...
//! ]
//! ```

use pathtracer::geometry::Ray;

pub use self::differential_geometry::DifferentialGeometry;
pub use self::bbox::BBox;
pub use self::bvh::BVH;

pub mod differential_geometry;
pub mod bbox;
pub mod bvh;
pub mod triangle;

/// Trait implemented by geometric primitives
pub trait Geometry {
    /// Test a ray for intersection with the geometry.
    /// The ray should have been previously transformed into the geometry's
    /// object space otherwise the test will be incorrect.
    /// Returns the differential geometry containing the hit information if the
    /// ray hit the object and set's the ray's `max_t` member accordingly
    fn intersect(&self, ray: &mut Ray) -> Option<DifferentialGeometry>;
}

/// Trait implemented by scene objects that can report an AABB describing their bounds
pub trait Boundable {
    /// Get an AABB reporting the object's bounds over the time period
    /// The default implementation assumes the object isn't animated and
    /// simply returns its bounds. This is kind of a hack to use
    /// the BVH for animated geomtry (instances) and non-animated geometry (triangles).
    fn bounds(&self, start: f32, end: f32) -> BBox;
}


pub trait BoundableGeom: Geometry + Boundable {}
impl<T: ?Sized> BoundableGeom for T where T: Geometry + Boundable {}

/// Enum representing on of the 3 spatial axes
#[derive(Copy, Clone, Debug)]
pub enum Axis {
    X = 0,
    Y = 1,
    Z = 2,
}
