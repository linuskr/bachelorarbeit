#version 330

in vec2 position;
in vec2 tex_coord;
in uint tex_id;

out vec2 f_tex_coord;
flat out uint f_tex_id;

void main() {
	f_tex_coord = tex_coord;
    f_tex_id = tex_id;
	gl_Position = vec4(position, 0.0, 1.0);
}