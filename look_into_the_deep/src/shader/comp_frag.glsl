#version 330

in vec2 f_tex_coord;
flat in uint f_tex_id;
            
uniform sampler2D tex_scene;
uniform sampler2D tex_fwd;
uniform sampler2D tex_bwd;

out vec4 color;

void main() {
	color = vec4(1.0, 0.0, 0.0, 1.0);
    switch (f_tex_id) {
        case 0u: 
            color = texture(tex_scene, f_tex_coord);
            break;
        case 2u:
            color = texture(tex_fwd, f_tex_coord);
            break;
        case 1u:
            color = texture(tex_bwd, f_tex_coord);
            break;
    }
}