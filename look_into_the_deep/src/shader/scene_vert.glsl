#version 330

in vec3 position;
in vec3 normal;
in vec2 tex_coord;

uniform mat4 modelview_matrix;
uniform mat4 proj_matrix;

out vec3 f_normal;
out vec2 f_tex_coord;

void main() {
	f_tex_coord = tex_coord;
    f_normal = mat3(modelview_matrix) * normal;
	gl_Position = proj_matrix * modelview_matrix * vec4(position, 1.0);
}