#version 330

in vec3 f_normal;
in vec2 f_tex_coord;
            
uniform sampler2D tex_ground;

out vec4 color;

void main() {
	if (f_normal == vec3(0.0)) {
        color = vec4(texture(tex_ground, f_tex_coord).rgb, 1.0);
    } else {
        color = vec4(0.0, 0.3, 0.0, 0.3);
    }
}