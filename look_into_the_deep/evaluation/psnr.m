function [ psnr_rgb, psnr_grey ] = psnr( input, output )
%UNTITLED Peak signal-to-noise ratio
%   Detailed explanation goes here
assert(isequal(size(input), size(output)));
[m, n, o] = size(input);
diff_rgb = input - output;
diff_rgb_sq = diff_rgb .^2;
err_rgb = sum(sum(sum(diff_rgb_sq))) / (m*n*o);
input_gray = rgb2gray(input);
output_gray = rgb2gray(output);
diff_gray = input_gray - output_gray;
diff_gray_sq = diff_gray .^2;
err_gray = sum(sum(diff_gray_sq)) / (m * n);
psnr_rgb = 20*log10(255)-10*log10(err_rgb);
psnr_grey = 20*log10(255)-10*log10(err_gray);
end

