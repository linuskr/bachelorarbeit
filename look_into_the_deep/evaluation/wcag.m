function [ rating ] = wcag( ref, restored )
%WCAG Weighted Contrast Average Grads
%   Detailed explanation goes here
w_0 = 0.8;
w_1 = 0.2;
alpha = 0.25;
ref = rgb2gray(ref);
refd = double(ref(:));
rest = rgb2gray(restored);
restd = double(rest(:));
grad_ref = imgradient(ref, 'central');
grad_rest = imgradient(rest, 'central');
[m, n] = size(grad_ref);
AG_0 = sum(sum(grad_ref)) / (m*n);
AG_k = sum(sum(grad_rest)) / (m*n);
F_con_0 = std(refd) / ((moment(refd, 4) / var(refd)^2)^alpha);
F_con_k = std(restd) / ((moment(restd, 4) / var(restd)^2)^alpha);
rating = w_0 * (1 - (AG_k/AG_0)) + w_1 * (1 - (F_con_k/F_con_0)); % flip because in the paper, _0 is the blurred version
end

