function [  ] = ior_plot( path )

fileID = fopen(strcat(path, 'ior_plot.txt'), 'w');

results = eval_dir(path);
len = length(results.filenames);
iors = [1.0, 1.3, 1.5, 1.7, 2.0, 2.5, 3.0];
%assert length(iors) == len;
fprintf(fileID, '%s %s', '%', path);
fprintf(fileID, '%s ', '%');
for i=1:len
    fprintf(fileID, '& %s  ', results.filenames{i});
end
fprintf(fileID, ' \n');

fprintf(fileID, '%sMSE\n', '%');

for i=1:len
    fprintf(fileID, '(%2g, %2g)\n', iors(i), results.mse(i));
end

fprintf(fileID, '%sSSIM\n', '%');

for i=1:len
    fprintf(fileID, '(%2g, %2g)\n', iors(i), results.ssim(i));
end

end