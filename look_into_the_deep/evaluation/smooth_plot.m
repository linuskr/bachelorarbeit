function [  ] = smooth_plot( path )

fileID = fopen(strcat(path, 'smooth_plot.txt'), 'w');
path = strcat(path, '0 -> ');
results = cell(1, 5);

for i=0:5
    p = strcat(path, [' ', int2str(i), '/'])
    results{i+1} = eval_dir(p);
end

fprintf(fileID, '%sMSE\n', '%');
for i=1:3
    i_ = results{1}.order(i);
    fprintf(fileID, '%s %s\n', '%', results{1}.refnames{i_});
    fprintf(fileID, '%s %s\n', '%', results{1}.filenames{i_});
    for j=0:5
        i__ = results{j+1}.order(i);
        fprintf(fileID, '(%2g, %2g)\n', j*10, results{j+1}.mse(i__));
    end
end

fprintf(fileID, '%sSSIM\n', '%');
for i=1:3
    i_ = results{1}.order(i);
    fprintf(fileID, '%s %s\n', '%', results{1}.refnames{i_});
    fprintf(fileID, '%s %s\n', '%', results{1}.filenames{i_});
    for j=0:5
        i__ = results{j+1}.order(i);
        fprintf(fileID, '(%2g, %2g)\n', j*10, results{j+1}.ssim(i__));
    end
end

fclose(fileID);
end