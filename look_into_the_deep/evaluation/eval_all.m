dirs = {'water_surface_Plane -> water_surface_Plane/', 'water_surface_Plane -> water_surface_smooth_1/', 'water_surface_Plane -> water_surface_smooth_2/', 'water_surface_Plane -> water_surface_smooth_3/', 'water_surface_Plane -> water_surface_smooth_4/', 'water_surface_Plane -> water_surface_smooth_5/'};
interpolation = { 'nearest/', 'linear/'};

ref = imread('flower.png');

hold all
subplot(4, 1, 1)
for i = 1:length(interpolation)
    ip = interpolation{i};
    mssim_means = zeros(1, length(dirs));
    mssim_stds = zeros(1, length(dirs));
    for j = 1:length(dirs)
        dir_ = dirs{j};
        path = strcat(dir_, ip);
        ev = eval_dir(ref, path);
        mssim_means(j) = 1 - ev.mssim_mean;
        mssim_stds(j) = ev.mssim_std;
    end
    errorbar(0:10:10*length(dirs)-1, mssim_means, mssim_stds)
end
axis([0 50 0 1])
xlabel('smoothness [%]')
ylabel('1 - mean SSIM')

subplot(4, 1, 2)
for i = 1:length(interpolation)
    ip = interpolation{i};
    means = zeros(1, length(dirs));
    stds = zeros(1, length(dirs));
    for j = 1:length(dirs)
        dir_ = dirs{j};
        path = strcat(dir_, ip);
        ev = eval_dir(ref, path);
        means(j) = ev.msqerr_mean;
        stds(j) = ev.msqerr_std;
    end
    errorbar(0:10:10*length(dirs)-1, means, stds)
end
axis([0 50 0 100])
xlabel('smoothness [%]')
ylabel('mean squared error')

subplot(4, 1, 3)
for i = 1:length(interpolation)
    ip = interpolation{i};
    means = zeros(1, length(dirs));
    stds = zeros(1, length(dirs));
    for j = 1:length(dirs)
        dir_ = dirs{j};
        path = strcat(dir_, ip);
        ev = eval_dir(ref, path);
        means(j) = ev.wcag_mean;
        stds(j) = ev.wcag_std;
    end
    errorbar(0:10:10*length(dirs)-1, means, stds)
end
axis([0 50 0 0.5])
xlabel('smoothness [%]')
ylabel('WCAG')

subplot(4, 1, 4)
for i = 1:length(interpolation)
    ip = interpolation{i};
    means = zeros(1, length(dirs));
    stds = zeros(1, length(dirs));
    for j = 1:length(dirs)
        dir_ = dirs{j};
        path = strcat(dir_, ip);
        ev = eval_dir(ref, path);
        means(j) = ev.nmi_mean;
        stds(j) = ev.nmi_std;
    end
    errorbar(0:10:10*length(dirs)-1, means, stds)
end
axis([0 50 0 0.5])
xlabel('smoothness [%]')
ylabel('NMI')
hold off
fig = gcf;
set(findall(fig,'-property','FontSize'),'FontSize',24);
