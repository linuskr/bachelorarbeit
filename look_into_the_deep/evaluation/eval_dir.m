function [ result ] = eval_dir( path )
%EVAL_DIR Summary of this function goes here
%   Detailed explanation goes here
ref_names = {'Checkerboard', 'Flower', 'Text'};
refs = {imread('../assets/ground1.png'), imread('../assets/flower.png'), imread('../assets/text.png')};
files = dir(strcat(path, '*bwd*'));
len = length(files);
mssim = zeros(len, 1);
msqerr = zeros(len, 1);
psnr_ = zeros(len, 1);
wcag_ = zeros(len, 1);
nmi_ = zeros(len, 1);
ref_name_idx = zeros(len, 1);
reference = refs{1};

for i=1:len
    filename = strcat(path, files(i).name);
    out = imread(filename, 'png');
    size_1 = size(out);
    found = false;
    for j=1:length(refs)
        size_2 = size(refs{j});
        if mod(size_1, size_2) == 0
            reference = refs{j};
            ref_name_idx(i) = j;
            found = true;
            break;
        end
    end
    assert(found);
    msqerr(i) = mean_sq_err(reference, out);
    psnr_(i) = psnr(reference, out);
    mssim(i) = ssim(reference, out);
    wcag_(i) = wcag(reference, out);
    nmi_(i) = nmi(double(reference(:)), double(out(:)));
end

reordered = cell(1, len);
filenames = cell(1, len);
for i = 1:len
    reordered(i) = ref_names(ref_name_idx(i));
    filenames(i) = {files(i).name};
end
order = zeros(1, len);
for i=1:len
    order(ref_name_idx(i)) = i;
end

result.order = order;
result.filenames = filenames;
result.refnames = reordered;
result.mse = msqerr;
result.psnr = psnr_;
result.ssim = mssim;
result.wcag = wcag_;
result.nmi = nmi_;
end

