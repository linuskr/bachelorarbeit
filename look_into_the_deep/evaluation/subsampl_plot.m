function [  ] = subsampl_plot( path )
path_ = path;
fileID = fopen(strcat(path, 'subsampl_plot.txt'), 'w');
path = strcat(path, '0 -> ');
results = cell(1, 5);
fprintf(fileID, '%s0:\n', '%');
len = 4;
for i=0:len
    p = strcat(path, [' ', int2str(i), '/'])
    results{i+1} = eval_dir(p);
end

fprintf(fileID, '%sMSE\n', '%');
for i=1:3
    i_ = results{1}.order(i);
    fprintf(fileID, '%s %s\n', '%', results{1}.refnames{i_});
    fprintf(fileID, '%s %s\n', '%', results{1}.filenames{i_});
    for j=0:len
        i__ = results{j+1}.order(i);
        fprintf(fileID, '(%2g, %2g)\n', j, results{j+1}.mse(i__));
    end
end

fprintf(fileID, '%sSSIM\n', '%');
for i=1:3
    i_ = results{1}.order(i);
    fprintf(fileID, '%s %s\n', '%', results{1}.refnames{i_});
    fprintf(fileID, '%s %s\n', '%', results{1}.filenames{i_});
    for j=0:len
        i__ = results{j+1}.order(i);
        fprintf(fileID, '(%2g, %2g)\n', j, results{j+1}.ssim(i__));
    end
end


path = strcat(path_, '5 -> ');
results = cell(1, 5);
fprintf(fileID, '\n\n%s5:\n', '%');
for i=0:len
    p = strcat(path, [' ', int2str(i+5), '/'])
    results{i+1} = eval_dir(p);
end

fprintf(fileID, '%sMSE\n', '%');
for i=1:3
    i_ = results{1}.order(i);
    fprintf(fileID, '%s %s\n', '%', results{1}.refnames{i_});
    fprintf(fileID, '%s %s\n', '%', results{1}.filenames{i_});
    for j=0:len
        i__ = results{j+1}.order(i);
        fprintf(fileID, '(%2g, %2g)\n', j, results{j+1}.mse(i__));
    end
end

fprintf(fileID, '%sSSIM\n', '%');
for i=1:3
    i_ = results{1}.order(i);
    fprintf(fileID, '%s %s\n', '%', results{1}.refnames{i_});
    fprintf(fileID, '%s %s\n', '%', results{1}.filenames{i_});
    for j=0:len
        i__ = results{j+1}.order(i);
        fprintf(fileID, '(%2g, %2g)\n', j, results{j+1}.ssim(i__));
    end
end

fclose(fileID);
end