function [] = make_table( path )

fileID = fopen(strcat(path, 'table.txt'), 'w');

results = eval_dir(path);
len = length(results.filenames);
fprintf(fileID, '%s %s', '%', path);
fprintf(fileID, '%s ', '%');
results.order = 1:len;
for i=1:len
    i_ = results.order(i);
    fprintf(fileID, '& %s  ', results.filenames{i_});
end
fprintf(fileID, ' \n');

for i=1:len
    i_ = results.order(i);
    fprintf(fileID, '& %s  ', results.refnames{i_});
end
fprintf(fileID, ' \\\\ \n\\hline\n MSE ');
for i=1:len
    i_ = results.order(i);
    fprintf(fileID, '& %2g  ', results.mse(i_));
end
fprintf(fileID, ' \\\\ \n PSNR ');
for i=1:len
    i_ = results.order(i);
    fprintf(fileID, '& %2g  ', results.psnr(i_));
end
fprintf(fileID, ' \\\\ \n SSIM ');
for i=1:len
    i_ = results.order(i);
    fprintf(fileID, '& %2g  ', results.ssim(i_));
end
fprintf(fileID, ' \\\\ \n WCAG ');
for i=1:len
    i_ = results.order(i);
    fprintf(fileID, '& %2g  ', results.wcag(i_));
end
fprintf(fileID, ' \\\\ \n NMI ');
for i=1:len
    i_ = results.order(i);
    fprintf(fileID, '& %2g  ', results.nmi(i_));
end
fclose(fileID);