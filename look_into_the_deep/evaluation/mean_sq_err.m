function [ err_rgb, err_gray ] = mean_sq_err( input, output )
%MEAN_SQ_ERR Calculates the mean squared error between two images
assert(isequal(size(input), size(output)));
[m, n, o] = size(input);
diff_rgb = input - output;
diff_rgb_sq = diff_rgb .^2;
err_rgb = sum(sum(sum(diff_rgb_sq))) / (m*n*o);
input_gray = rgb2gray(input);
output_gray = rgb2gray(output);
diff_gray = input_gray - output_gray;
diff_gray_sq = diff_gray .^2;
err_gray = sum(sum(diff_gray_sq)) / (m * n);
end

