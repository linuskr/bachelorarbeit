% Diplomarbeitsvorlage, basierend auf ubicomp-long.cls von Stefan Dietzel
% (http://www.vf.utwente.nl/~dietzels/) und der Vorlage von Guido de Melo (http://guido.demelo.de)
% Zusammengestellt von Jonas Beinder (http://www.uni-ulm.de/in/mi/mitarbeiter/jonas-beinder.html)
\ProvidesClass{diplom-mi}[2011/02/10 v0.1a Diplomarbeitsvorlage Medieninformatik]

\RequirePackage[T1]{fontenc}
\usepackage[pagewise,modulo]{lineno}

\DeclareOption{mi-draft}{
  \PassOptionsToPackage{backgroundcolor=orange}{todonotes}
  \PassOptionsToPackage{pdfborder={0 0 0.5}}{hyperref}
  \PassOptionsToPackage{final}{showkeys}
  \ClassWarning{myclass}{Draft Mode.}
  \linenumbers
}
\DeclareOption{mi-draft-verbose}{
  \PassOptionsToPackage{backgroundcolor=orange}{todonotes}
  \PassOptionsToPackage{pdfborder={0 0 0.5}}{hyperref}
  \ClassWarning{myclass}{Draft Mode.}
  \linenumbers
}
\DeclareOption{mi-release-electronic}{
  \PassOptionsToPackage{disable}{todonotes}
  \PassOptionsToPackage{pdfborder={0 0 0.5}}{hyperref}
  \PassOptionsToPackage{final}{showkeys}
}
\DeclareOption{mi-release-print}{
  \PassOptionsToPackage{disable}{todonotes}
  \PassOptionsToPackage{pdfborder={0 0 0}}{hyperref}
  \PassOptionsToPackage{final}{showkeys}
}
\ProcessOptions\relax

\LoadClass[
	a4paper,	% A4
	titlepage,	% Mit Titelseite
	DIV=9,		% Text-Randverhältnis 6:3
	BCOR=10mm,	% Binding Correction
	twoside		% Zweiseitig
]{scrbook}

\RequirePackage[raiselinks=true,
    pdftex,
    colorlinks=false,
	bookmarks=true,
	bookmarksopenlevel=1,
	bookmarksopen=true,
	bookmarksnumbered=true,
	hyperindex=true,
	plainpages=false,
	pdfpagelabels=true,
	linkbordercolor={0 0.61 0.50},
    draft=false,
	citebordercolor={0 0.61 0.50}]{hyperref}
                        
%\RequirePackage[
%	pdftex,
%	colorlinks=true,
%	linkcolor=black,
%	citecolor=black,
%	urlcolor=black
%]{hyperref}		% Links im PDF

\RequirePackage{iflang}
\RequirePackage{scrhack}
\RequirePackage[sc]{mathpazo}		% Palatino
\RequirePackage{helvet}				% Helvetica
\RequirePackage{booktabs}			% Schönere Tables
\RequirePackage[final,draft=false]{microtype}			% Micro-Typographische Verbesserungen
\RequirePackage{graphicx}			% Grafiken
\RequirePackage{setspace}			% Zeilenabstand
\RequirePackage[utf8]{inputenc}		% Codierung - Achtung! Wenn eine andere Codierung als UTF-8 verwendet wird, muss dies hier angegeben werden!
\RequirePackage[numbers]{natbib}	% Quellenformatierung
\RequirePackage{tikz}

\RequirePackage{todonotes}
\usepackage[color]{showkeys}

\def\normalsize{\@setfontsize{\normalsize}{10}{12.00pt}}	% 10 pt Schriftgröße
\normalsize
\abovedisplayskip 1.5ex plus4pt minus2pt
\belowdisplayskip \abovedisplayskip
\abovedisplayshortskip 0pt plus4pt
\belowdisplayshortskip 1.5ex plus4pt minus2pt

\RequirePackage[font=sf,sf]{caption}   	% immer Sans-Serif Font für Bildunterschriften
\RequirePackage[sf,SF]{subfigure}		


\@ifpackagewith{babel}{ngerman}{\def\abstractname{Zusammenfassung}}{\def\abstractname{Abstract}}
\newenvironment{abstract}{
	\cleardoublepage
	\section*{\abstractname}
}
{\vfill\cleardoublepage}

\def\@type{}
\newcommand\type[1]{\def\@type{#1}}
\def\@jahr{}
\newcommand\jahr[1]{\def\@jahr{#1}}
\def\@matnr{}
\newcommand\matnr[1]{\def\@matnr{#1}}
\def\@emailA{}
\newcommand\emailA[1]{\def\@emailA{#1}}
\def\@emailB{}
\newcommand\emailB[1]{\def\@emailB{#1}}
\def\@emailC{}
\newcommand\emailC[1]{\def\@emailC{#1}}
\def\@authorA{}
\newcommand\authorA[1]{\def\@authorA{#1}}
\def\@authorB{}
\newcommand\authorB[1]{\def\@authorB{#1}}
\def\@authorC{}
\newcommand\authorC[1]{\def\@authorC{#1}}
\def\@fakultaet{}
\newcommand\fakultaet[1]{\def\@fakultaet{#1}}
\def\@institut{}
\newcommand\institut[1]{\def\@institut{#1}}
\def\@gutachterA{}
\newcommand\gutachterA[1]{\def\@gutachterA{#1}}
\def\@gutachterB{}
\newcommand\gutachterB[1]{\def\@gutachterB{#1}}
\def\@betreuer{}
\newcommand\betreuer[1]{\def\@betreuer{#1}}

% Titelseite
\renewcommand\maketitle{%
	\thispagestyle{empty}
	{	
		\begin{addmargin*}[-4mm]{-24mm}
			\includegraphics[height=1.8cm]{images/unilogo_bild}
			\hfill
			\includegraphics[height=1.8cm]{images/unilogo_wort}
			\vspace{.5cm}
			%\hspace*{100.5mm}\parbox[t]{60mm}{
				\begin{flushright}
                    \IfLanguageName{ngerman}{
					\textsf{\bfseries Universit\"at Ulm \textbar \mdseries ~89069 Ulm \textbar~Germany}\\\vspace{1.25cm}
					\bfseries Fakult\"at für Ingenieurwissenschaften, \\Informatik und Psychologie\\ \mdseries Institut f\"ur Medieninformatik
                    }{
					\textsf{\bfseries Ulm University \textbar \mdseries ~89069 Ulm \textbar~Germany}\\\vspace{1.25cm}
					\bfseries Faculty of Engineering, \\Computer Science and Psychology\\ \mdseries Institute of Media Informatics
                    }
				\end{flushright}
			%}
			\vspace{2cm}
			\normalfont\Huge\bfseries\@title\\[.25cm]
			\normalsize
				\mdseries\@type~\IfLanguageName{ngerman}{an der Universit\"at Ulm}{at Ulm University}\\[1cm]
				\bfseries \IfLanguageName{ngerman}{Vorgelegt von}{Presented by}:\\
				\mdseries\@authorA\\
				\@emailA\\[2.5cm]
				\bfseries \IfLanguageName{ngerman}{Gutachter}{Examiner}:\\
				\mdseries\@gutachterA\\
				\@gutachterB\\[.5cm]
				\bfseries \IfLanguageName{ngerman}{Betreuer}{Advisor}:\\
				\mdseries\@betreuer\\[1cm]
				\bfseries \@jahr
			
		\end{addmargin*}
	}
}

\newcommand\copyrightinfo{
	\copyright~\@jahr~\@author\\[.5em]
    \IfLanguageName{ngerman}{
    Diese Arbeit ist lizensiert unter der Creative Commons
	\textbf{Namensnennung-Keine kommerzielle Nutzung-Weitergabe unter gleichen Bedingungen
	3.0 Deutschland} Lizenz. N\"ahere Informationen finden Sie unter
	\url{http://creativecommons.org/licenses/by-nc-sa/3.0/de/}.
    }{
    This work is licensed under the Creative Commons \textbf{Attribution-NonCommercial-ShareAlike 3.0 Unported} License.
    To view a copy of this license, visit \url{http://creativecommons.org/licenses/by-nc-sa/3.0/}.
    }
}

\newcommand\declaration{
	\ihead{\IfLanguageName{ngerman}{Name}{Name}: \@author}
	\ohead{\IfLanguageName{ngerman}{Matrikelnummer}{Matriculation Number}: \@matnr}
	\ofoot{\pagemark}	
	\vspace*{2cm}	
	\minisec{\IfLanguageName{ngerman}{Erklärung}{Declaration}}
	\vspace{.5cm}
    \IfLanguageName{ngerman}{
    Ich, \@author, Matrikelnummer \@matnr, erkl\"are, dass ich die Arbeit
	selbst\"andig verfasst und keine anderen als die angegebenen Quellen und
	Hilfsmittel verwendet habe.}{
    I, \@author, matriculation number \@matnr, hereby declare that I created this work on my own. 
	Except where referenced and credited to the original author, I have not used the material of others in my work.}\\[1cm]
	\hspace*{2cm} \IfLanguageName{ngerman}{Ulm, den}{Ulm,} \dotfill\\
	\hspace*{10cm} {\footnotesize \@author}
}

\newcommand\copyrightpage{
    \clearpage
    \thispagestyle{empty}
    {	\small
        \flushleft
        ~\vfill
        \textsf{\IfLanguageName{ngerman}{Fassung vom}{Last updated} \today\\[1cm]
        \copyrightinfo\\[.5cm]
        \IfLanguageName{ngerman}{Satz}{Typesetting}: PDF-\LaTeXe}
    }
}

\RequirePackage{scrpage2}
\pagestyle{scrheadings}
\clearscrheadfoot
\automark[section]{chapter}

\rohead{\MakeUppercase{\rightmark}}
\lehead{\MakeUppercase{\leftmark}}
\ofoot{\pagemark}

\setkomafont{pageheadfoot}{%
\normalfont\sffamily\scshape\footnotesize
}
\setkomafont{pagenumber}{%
\normalfont\sffamily\footnotesize
}

% Schusterjungen / Hurenkinder vermeiden
\widowpenalty10000
\clubpenalty10000

% Bis zu welcher Tiefe kommen Überschriften ins Inhaltsverzeichnis
\setcounter{tocdepth}{2}

