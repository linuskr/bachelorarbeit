%% content.tex
%%

%% ==============================
\chapter{Image Undistortion System}
\label{ch:content}
%% ==============================
In this chapter a model for a scene containing a camera, at least one interface between media with different indices of refraction and some opaque object is described. Then, a new technique for undistortion of images taken in such a scene is discussed.

\section{Abstract Representation of the Scene}
\label{ch:content:sec:models}

\paragraph{The Camera}
\label{ch:content:sec:models:par:camera}
The camera is modeled as a pinhole camera with aperture $ \mathit{f}/\infty $. 
To specify the camera's position and orientation, the position of the pinhole $ \point{p_{pinhole}} $, the normal of the image plane $\vect{n}$ and the respectively orthogonal up $\vect{u}$ and right $\vect{r}$ vectors are chosen, which form the camera's coordinate frame. 
The image plane is then discretized into individual pixels according to the resolution $\vect{a}$. 
To approximate the two dimensional area of the pixel, a for each ray randomly selected offset $\vect{t}_j \in [-0.5, 0.5]^2$ is applied to index of the pixel $\point{i} \in \{(x, y) \in \mathbb{N}_0^2 \mid x < \vect{a}_x \wedge y < \vect{a}_y\}$.
Then, an offset is applied to make the position relative to the center of the image.
Scaled by the pixel size $\vect{s}$, this yields the position of the pixel on the image plane:
\begin{align}
\point{p}_{\point{i}_j} &= (\point{i} + \vect{t}_j - \frac{\vect{a}}{2}) \odot \vect{s}
\intertext{where $\odot$ is the Hadamard product, i.e. the element-wise vector multiplication. The pixel size is calculated from the field of view $f$ and the aspect ratio $r = \frac{\vect{a}_y}{\vect{a}_x}$:}
\vect{s} &= 2\cdot\tan\left(\frac{f}{2}\right) \cdot \begin{pmatrix}
1 \\
r
\end{pmatrix}
\odot \frac{\vect{1}}{\vect{a}}
\intertext{It describes the extent of each pixel on the image plane. Using the pixel position, the direction vector of the ray can be calculated:}
\vectgr{\omega}'_{\point{i}_j} &= \vect{n} - (\point{p}_{\point{i}_j} \cdot \vect{e}_1) \cdot \vect{r} - (\point{p}_{\point{i}_j} \cdot \vect{e}_2) \cdot \vect{u}
\intertext{$\vect{e}_i$ denotes the unit vector with $1$ in the $i$th component. The normalized direction vector $\vectgr{\omega}_{\point{i}_j} = \frac{\vectgr{\omega}'_{\point{i}_j}}{||\vectgr{\omega}'_{\point{i}_j}||}$ and the pinhole as origin then specify the initial ray}
\vect{r}_{\point{i}_j} &= (\point{p}_{pinhole}, \vectgr{\omega}_{\point{i}_j}). \label{eq:init_ray}
\end{align}


\paragraph{Interfaces Between Media with Different Indices of Refraction}
\label{ch:content:sec:models:par:boundaries}
To keep the path tracing and collision test calculations reasonably simple and performant, the index of refraction is not stored as a continuous volume; 
instead, an approach similar to isosurfaces is used: 
Usually the index of refraction only changes significantly at the interface between two media.
This is the case because in most scenarios there's a strict segregation between the different media, e.g. at a water surface. 
Thus, the index of refraction at a point can be approximated as a step function, i.e. a piecewise constant function, that only changes at the interfaces.
As a result, it is enough to store those interfaces and the index of refraction $n$ for each respective side. 
If the interfacing media are soluble in each other or mix in other ways, and there is no hard boundary, the variation can be approximated arbitrarily fine by discretizing it into layered boundary surfaces.\\
The interface is then stored as a triangle mesh with per-vertex normals.

\paragraph{Opaque Objects}
\label{ch:content:sec:models:par:objects}
Visible objects, e.g. the ground beneath water, are modeled as textured triangle meshes. 
As this model does not consider the frequency distribution of the light and intends to be reversible, reflexion and scattering for opaque objects is not taken into account, though the former can be emulated by adding an index of refraction interface with the desired properties right above the object.

\section{Tracing of Light Paths through the Scene to Recover the Ground Truth}
\label{ch:content:sec:tracing}
The previously described model of the scene is now the input to a path tracer.
Normally, the camera would be the sensor, and the ground beneath the index of refraction interfaces would be the emitter.
To reconstruct the undistorted ground texture, the emitter and sensor are switched compared to the ordinary case.
With this reversal of roles, where the camera becomes the emitter and the ground is the sensor, the distorted image is projected back into the scene, similar to a diascope.
Because of the reciprocity discussed in Section~\ref{ch:basics:sec:reciprocity}, basic radiance and importance are transported identically, thus the paths and their throughput are invariant under the exchange of sensor and emitter.
This allows the path sampling process to be identical to a normal path tracer.

Due to the delta distributions in the BSDF, most paths have zero throughput.
As only paths with non-zero throughput contribute anything to the result, it is very desirable to sample mainly these paths.
The easiest way to do that is to incrementally construct paths starting at the camera, as the pinhole is very difficult to hit otherwise.
Let $\muspace{B} \subset \muspace{S}$ be the set of index of refraction interface points and $\muspace{G} \subset \muspace{S}$ the set of ground points. 
Obviously, $\muspace{B} \cap \muspace{G} = \emptyset$ and $\muspace{B} \cup \muspace{G} = \muspace{S}$.
Let $P$ be a random variable uniformly distributed in $[0, 1]$.
The incremental path construction then follows these steps:

\begin{enumerate}
	\item The initial ray $\vect{r}_0 = (\point{p}_0, \vectgr{\omega}_0)$ through $\point{i}$ is given by the camera as specified in \eqref{eq:init_ray}. The initial throughput $\tau_0$ of the path is $1$.
	\item\label{start} The current ray $\vect{r}_i$ is intersected with the scene. If nothing is hit, the path can be discarded. Otherwise, let $\point{x}_i \in \muspace{S}$ be the intersection point.
	\item If $\point{x}_i \in \muspace{G}$, the path is completed and $\point{x}_{End} = \point{x}_i$, $\tau_{End} = \tau_i$. \\
	Otherwise, $\point{x}_i \in \muspace{B}$. The BSDF is then evaluated at the intersection point in direction of the reflected vector $\vectgr{\omega}_r = \mat{H}(\point{x}) \vectgr{\omega}_i$:
	\begin{align}
		\beta_r &= f_s(\point{x}_i, \vectgr{\omega}_i, \vectgr{\omega}_r)
		\intertext{Next, let $p_i$ be a sample of $P$. This is the origin of the stochastic properties of the algorithm. The next ray is then given by}
		\vect{r}_{i+1} &= \begin{cases}
			(\point{x}_i, \vectgr{\omega}_r) &\qquad p \leq \beta_r \\
			(\point{x}_i, \vectgr{\omega}_t) &\qquad \text{otherwise.}
		\end{cases}
		\intertext{where $\vectgr{\omega}_t$ is the transmitted vector given by equation \eqref{eq:snell_vector}. Similar, the throughput is updated:}
		\tau_{i+1}' &= \begin{cases}
			\tau_i \cdot \beta_r &\qquad p \leq \beta_r \\
			\tau_i \cdot (1 - \beta_r) &\qquad \text{otherwise.}
		\end{cases}
	\end{align}
	Total internal reflection does not have to be handled separately, as the BSDF evaluates to $1$ there.
	\item Finally, Russian Roulette is performed. Let $p'_i$ be a different sample of $P$ and $s = \min\{0.9; \tau_{i+1}'\}$ the survival factor.
	If $p > s$, the current path is discarded. Otherwise, let $\tau_{i+1} = \frac{\tau_{i+1}'}{s}$.
	\item Return to Step~\ref{start}
\end{enumerate}
If it is not discarded at one step, this algorithm yields a path from $\point{x}_{Start}$ to $\point{x}_{End}$ with throughput $\tau_{End}$.
This single-emitter property is caused by the non-reflectivity of the emitter.

Let $\point{c} = (r, g, b, 1)$ be the color of the distorted image at $\point{i}$ with an additional component to track the cumulative throughput, and $\mathrm{uv}: \muspace{G} \to [0, 1]^2$ the uv-mapping of the ground mesh.
The undistorted image $U$ with resolution $\vectgr{\chi}$ is then generated depending on the interpolation method.
If no interpolation is done, the pixel coordinates are simply rounded towards the nearest integer.
$\lfloor\cdot\rceil$ is used to denote the rounding function.
\begin{align}
	(u, v) &= \mathrm{uv}(\point{x}_{End})\odot\vectgr{\chi} \notag
	\\
	U^{(i+1)}_{\lfloor u \rceil, \lfloor v \rceil} &= U^{(i)}_{\lfloor u \rceil, \lfloor v \rceil} + \point{c} \cdot \tau_{End} \notag
	\intertext{For linear interpolation, the contribution of the path is distributed among the four closest pixels. First, the fractional part of a number is given by the sawtooth function}
	\{x\} &= x - \lfloor x \rfloor.
	\intertext{Using the ceiling, floor and fractional functions, the evolution of $U$ is given by}
	U^{(i+1)}_{\lceil u \rceil, \lceil v \rceil} &= U^{(i)}_{\lceil u \rceil, \lceil v \rceil} + \point{c} \cdot \tau_{End} \cdot \{u\} \cdot \{v\} \notag
	\\
	U^{(i+1)}_{\lfloor u \rfloor, \lceil v \rceil} &= U^{(i)}_{\lfloor u \rfloor, \lceil v \rceil} + \point{c} \cdot \tau_{End} \cdot (1 - \{u\}) \cdot \{v\} \notag
	\\
	U^{(i+1)}_{\lceil u \rceil, \lfloor v \rfloor} &= U^{(i)}_{\lceil u \rceil, \lfloor v \rfloor} + \point{c} \cdot \tau_{End} \cdot \{u\} \cdot (1 - \{v\}) \notag
	\\
	U^{(i+1)}_{\lfloor u \rfloor, \lfloor v \rfloor} &= U^{(i)}_{\lfloor u \rfloor, \lfloor v \rfloor} + \point{c} \cdot \tau_{End} \cdot (1 - \{u\}) \cdot (1 - \{v\}). \notag
\end{align}
To finalize the undistorted image, its pixel's components have to be mapped into the $[0, 1]^4$ interval. 
This is necessary because not only is more than one path per pixel of the distorted image evaluated, which would yield a constant factor, but the paths of many distorted pixels may converge on a single pixel in the undistorted image, an effect similar to caustics.
With the extra component for cumulative throughput, the color is scaled and the final pixel value is obtained:
\begin{equation}
	U_{u, v} = U^{(n)}_{u, v} \cdot \frac{1}{\vect{e}_4 \cdot U^{(n)}_{u, v}}
\end{equation}

\chapter{Implementation}
\label{ch:impl}

\section{Acceleration Data Structure}
\label{ch:impl:sec:ads}
In a naive raytracing algorithm, every ray would have to be tested for intersection with every triangle in the scene, resulting in $ \mathcal{O}(kn) $ intersection tests, where $k$ is the number of rays and $n$ is the number of triangles. 
For a frame of a scene with 32000 triangles and FullHD resolution at 16 samples per pixel, $ 1920 \cdot 1080 \cdot 16 \cdot 32000 = 1061683200000 \approx 1,1 \cdot 10^{12}$ intersection tests would be necessary. 
Clearly, it is essential to reduce the number of intersection tests, and perform them as fast as possible. 
To achieve this, several techniques are employed:

\paragraph{Bounding Volume Hierarchies}
\label{ch:impl:sec:models:par:bvh}
As usually only a small part of the scene intersects with any given ray, only a fraction of these test is actually necessary. 
To skip unnecessary intersection tests, a spatial data structure is used to sort the objects in the scene. 
One popular spatial data structure is the bounding volume hierarchy (BVH)~\cite{arvo1989survey, goldsmith1987automatic}. 
A BVH is a tree-like structure where every node has an associated bounding volume, and the bounding volume of the parent contains at least the union of all its children's bounding volumes. 
In this case, axis-aligned bounding boxes (AABBs) are used as bounding volumes.

\paragraph{Axis-Aligned Bounding Boxes} are cuboids whose edges are parallel to the cartesian coordinate axes.
They can both be generated and intersected with a ray very easily and fast.
An AABB is defined by two opposing corner points $\point{p_1}$ and $\point{p_2}$, which can be calculated for a set of points $P$ with the $\min$ and $\max$ functions. Let $\point{x}^{(i)} = \vect{e}_i\cdot \point{x}$.
\begin{align}
	\point{p_1} &= \begin{pmatrix}
		\min\{\point{x}^{(1)} | \point{x} \in P \} \\
		\min\{\point{x}^{(2)} | \point{x} \in P \} \\
		\min\{\point{x}^{(3)} | \point{x} \in P \}
	\end{pmatrix}
	\\
	\point{p_2} &= \begin{pmatrix}
	\max\{\point{x}^{(1)} | \point{x} \in P \} \\
	\max\{\point{x}^{(2)} | \point{x} \in P \} \\
	\max\{\point{x}^{(3)} | \point{x} \in P \}
	\end{pmatrix}
\end{align}

\section{Intersection Testing}
\label{ch:impl:sec:intersect}
The main advantage of AABBs is the fast intersection test against a ray. This is achieved by clipping the ray against each of the bounding planes. The tuple $(\point{o}, \vectgr{\omega})$ describing a ray defines the half-line $\{ \point{o} + t\cdot\vectgr{\omega} | t \in \mathbb{R}^+ \}$. Let the AABB be given by $\point{p_1}, \point{p_2}$. Define for $i=1,2,3$
\begin{align}
	t_{min}^{(i)} &= \begin{dcases}
		\frac{\point{p_1}^{(i)} - \point{o}^{(i)}}{\vectgr{\omega}^{(i)}} &\qquad \sgn(\vectgr{\omega}^{(i)}) = 1 \\
		\frac{\point{p_2}^{(i)} - \point{o}^{(i)}}{\vectgr{\omega}^{(i)}} &\qquad \text{otherwise}
	\end{dcases}
	\\
	t_{max}^{(i)} &= \begin{dcases}
		\frac{\point{p_2}^{(i)} - \point{o}^{(i)}}{\vectgr{\omega}^{(i)}} &\qquad \sgn(\vectgr{\omega}^{(i)}) = 1 \\
		\frac{\point{p_1}^{(i)} - \point{o}^{(i)}}{\vectgr{\omega}^{(i)}} &\qquad \text{otherwise.}
	\end{dcases}
\end{align}
The ray intersects the AABB if and only if 
\begin{equation}
	\min\left\{t_{min}^{(i)} | i=1,2,3 \right\} \leq \max\left\{t_{max}^{(i)} | i=1,2,3 \right\}.
\end{equation}
When IEEE 754 floating point numbers are used, the case $\vectgr{\omega}^{(i)} = 0$ does not have to be treated differently, as division by zero is defined there~\cite{smits2005efficiency}.


Once a leaf in the BVH is reached, all triangles contained have to be tested for intersection.
This ray-triangle-intersection is performed using the Möller-Trumbore-algorithm~\cite{moller2005fast}. \\
Let the ray be defined by $\{ \point{o} + t\cdot\vectgr{\omega} | t \in \mathbb{R}^+ \}$ as above, and the triangle by its vertices $\point{p_1}, \point{p_2}, \point{p_3}$.
The points on the triangle are then given by $(1 - \alpha - \beta) \cdot\point{p_1} + \alpha \cdot\point{p_2} + \beta \cdot\point{p_3}$, where $\alpha, \beta \geq 0, \alpha + \beta \leq 1$ are called barycentric coordinates.
An intersection between ray and triangle thus satisfies the equation
\begin{equation}
	\point{o} + t\cdot\vectgr{\omega} = (1 - \alpha - \beta) \cdot\point{p_1} + \alpha \cdot\point{p_2} + \beta \cdot\point{p_3}.
\end{equation}
\begin{equation}
	\eq \begin{bmatrix} -\vectgr{\omega}, & \point{p_2} - \point{p_1}, & \point{p_3} - \point{p_1}\end{bmatrix} 
	\cdot 
	\begin{bmatrix} t \\ \alpha \\ \beta \end{bmatrix} = \point{o} - \point{p_1}
\end{equation}
The solution to this linear equation system can be calculated using Cramer's rule.
To improve readability, let $\vect{v_1} = \point{o} - \point{p_1}$, $\vect{v_2} = \point{p_2} - \point{p_1}$ and $\vect{v_3} = \point{p_3} - \point{p_1}$.
\begin{equation}
	\eq \begin{bmatrix} t \\ \alpha \\ \beta \end{bmatrix} = 
	\frac{1}{\begin{vmatrix} -\vectgr{\omega}, & \vect{v_2}, & \vect{v_3} \end{vmatrix}} \cdot \begin{bmatrix}
		|\hspace{1em} \vect{v_1}, & \vect{v_2}, & \vect{v_3} | \\
		| -\vectgr{\omega}, & \vect{v_1}, & \vect{v_3} | \\
		| -\vectgr{\omega}, & \vect{v_2}, & \vect{v_1} |
	\end{bmatrix} \label{eq:mt_det}
\end{equation}
The determinants can be calculated using the scalar triple product, which obeys the identity
\begin{equation}
	\begin{vmatrix} \vect{a}, & \vect{b}, & \vect{c} \end{vmatrix} = \vect{a} \cdot (\vect{b} \times \vect{c}) = -(\vect{a} \times \vect{c}) \cdot \vect{b} = -(\vect{c} \times \vect{b}) \cdot \vect{a}.
\end{equation}
Equation \eqref{eq:mt_det} can now be rewritten as
\begin{equation}
\begin{bmatrix} t \\ \alpha \\ \beta \end{bmatrix} = 
\frac{1}{(\vectgr{\omega} \times \vect{v_3}) \cdot \vect{v_2}} \cdot \begin{bmatrix}
	(\vect{v_1} \times \vect{v_2}) \cdot \vect{v_3} \\
	(\vectgr{\omega} \times \vect{v_3}) \cdot \vect{v_1} \\
	(\vect{v_1} \times \vect{v_2}) \cdot \vectgr{\omega}
\end{bmatrix}. \label{eq:mt_vec}
\end{equation}
The triangle is intersected by the ray if and only if $(\alpha, \beta)$ are valid barycentric coordinates, i.e. $\alpha, \beta \geq 0, \alpha + \beta \leq 1$. 
The intersection along the ray is then given by $t$.
Thus, the first intersection along the ray is the valid intersection with the smallest $t$.

The normal and uv-coordinates can be interpolated using the barycentric coordinates and their values for the vertices:
\begin{equation}
	\vect{n} = (1 - \alpha - \beta) \cdot \vect{n}(\point{p_1}) + \alpha \cdot \vect{n}(\point{p_2}) + \beta \cdot \vect{n}(\point{p_3})
\end{equation}

Because the index of refraction interfaces are stored as triangles with the indices of refraction for each side, it is necessary to determine which side of the triangle was hit.
The per-vertex normals cannot be used for this, as they can give wrong results.
Instead, the normal of the triangle plane has to be used.
The side which was hit is then given by the sign of the cosine of the angle between the ray direction and the normal:
\begin{equation}
	\sgn(\vectgr{\omega} \cdot (\vect{v_2} \times \vec{v_3})) = sgn((\vectgr{\omega} \times \vect{v_3}) \cdot \vect{v_2})
\end{equation}
Note that this determinant has already been calculated in \eqref{eq:mt_vec}.

\section{Parallelization}
\label{ch:impl:sec:parallelization}
Path tracing is an extremely parallel problem.
As a path does not depend on other paths, each can be constructed in parallel.
In practice, there are generally a lot more paths to be constructed than hardware execution threads available.
To reduce scheduling overhead, a bunch of paths are pooled into one render job.
While it would be optimal to partition the sensor surface, as it is mutated, this is not possible, as only the start of the path is known initially.
Thus, the emitter surface, which is the camera image plane, is partitioned into chunks.
The chunks are then processed in parallel by a work-stealing thread pool.
This does not completely solve the problem of the concurrently mutated ground surface though, which stores the results.

The easiest way to synchronize the concurrent writes to the result image would be to put it behind a mutex.
Obviously, this is not a solution though, as it limits parallelism extremely.

A more fine-grained approach would be to atomically update each pixel in the result image.
While better than the global mutex, this approach also has some problems, mainly due to the second-class nature of floating point numbers in the x86-architecture.
While integers have atomic add instructions, due to the separate nature of the FPU, there is no such thing for floating point numbers.
Instead, they have to be added in a compare-and-swap loop, where the previous value is loaded, the value is incremented, and swapped if the value in memory is still the same as the loaded previous value.
If it is not, the addition has to be restarted and tried again.
This issue is exacerbated by the delay caused by the move to and from the FPU in each attempt.
Additionally, and not exclusive to floating point numbers, frequent writes to the same memory area by different CPU cores increase cache pressure, as the respective memory has to be flushed out of the core local caches every time a different core accesses it.

For a limited number of cores, like in a CPU, the best solution is to give each thread its own ground image.
This way, the writes do not have to be synchronized, and all threads can write freely to their local storage.
The disadvantage of this approach is the slightly increased memory consumption and the addition of a combination operation on completion, where the local ground images are merged into the final result.
This final combination is very fast though, both each operation and due to the very limited number of threads.
